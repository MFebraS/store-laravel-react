<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Exception;
use Throwable;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * Register the exception handling callbacks for the application.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->reportable(function (Throwable $e) {
			//
		});

		$this->renderable(function (Exception $e, $request) {
	        return $this->customHandler($e, $request);
	    });
	}

	private function customHandler($e, $request)
	{
		if (($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException) && $request->wantsJson()) {
			$messages = ['Something went wrong.'];
			// get model name
			preg_match('/\[([^[\]]+)/', $e->getMessage(), $output);
			if (isset($output[1])) {
				$explode = explode('\\', $output[1]);
				$model_name = end($explode);
				$messages = [$model_name . ' not found.'];
			}
			elseif ($e instanceof NotFoundHttpException) {
				$messages = ['Url not found.'];
			}

			return response()->json([
				'messages' => $messages
			], 404);
		}

		if ($e instanceof MethodNotAllowedHttpException && $request->wantsJson()) {
			return response()->json([
				'messages' => ['Method not allowed']
			], 400);
		}
	}
}
