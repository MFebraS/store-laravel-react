<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ApiValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if(method_exists($response, 'getData')) {
            // if validation fail, remake the json
            $response_data = $response->getData();
            if (isset($response_data->errors)) {
                $errors = [];
                foreach ($response_data->errors as $key => $value) {
                    $errors = array_merge($errors, $value);
                }

                return Response::json([
                    'messages' => $errors
                ], 400);
            }
        }

        return $response;
    }
}
