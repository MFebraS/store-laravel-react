<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\Models\User;

class AuthController extends Controller
{
	public function register(Request $request)
    {
    	$request->validate([
	        'name' 	   	  => 'required',
	        'email'    	  => 'required|email|unique:users,email',
            'phone'    	  => 'required|numeric|digits_between:10,15',
	        'password' 	  => 'required|min:6|confirmed',
	        'device_name' => 'required',
	    ]);

	    $user = User::create([
	    	'name' 	   => $request->name,
	    	'email'    => $request->email,
	    	'phone'    => $request->phone,
	    	'password' => bcrypt($request->password),
	    	'type' 	   => 'Customer',
            'status'   => 'Active',
	    ]);

	    $token = $user->createToken($request->device_name)->plainTextToken;

        return response()->json([
            'status' => 'success',
            'data'	 => [
	    		'token' => $token,
	    		'user'  => [
	    			'name' 			=> $user->name,
	    			'email' 		=> $user->email,
	    			'phone' 		=> $user->phone,
	    			'device_token'  => $user->device_token,
	    			'register_date' => dateFormat($user->created_at, 'full'),
	    		]
	    	]
        ]);
    }
    
    public function login(Request $request)
    {
    	$request->validate([
	        'email' 	  => 'required|email',
	        'password' 	  => 'required|min:6',
	        'device_name' => 'required',
	    ]);

	    $user = User::where('email', $request->email)->where('type', 'Customer')->firstOrFail();

	    if (!Hash::check($request->password, $user->password)) {
	        throw ValidationException::withMessages([
	            'email' => ['The provided credentials are incorrect.'],
	        ]);
	    }

	    $token = $user->createToken($request->device_name)->plainTextToken;

	    return response()->json([
	    	'status' => 'success',
	    	'data'	 => [
	    		'token' => $token,
	    		'user'  => [
	    			'name' 			=> $user->name,
	    			'email' 		=> $user->email,
	    			'phone' 		=> $user->phone,
	    			'device_token'  => $user->device_token,
	    			'register_date' => dateFormat($user->created_at, 'full'),
	    		]
	    	]
	    ]);
    }

    public function logout()
    {
    	$user = auth()->user();
    	$delete = $user->tokens()->delete();

    	if ($delete > 0) {
    		return response()->json([
    			'status'   => 'success',
    			'messages' => ['Logout success.']
    		]);
    	}

		return response()->json([
			'messages' => ['Logout failed.']
		], 400);
    }
}
