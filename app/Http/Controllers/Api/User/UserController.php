<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        
        return response()->json([
            'status' => 'success',
            'data'   => [
                'name'          => $user->name,
                'email'         => $user->email,
                'phone'         => $user->phone,
                'device_token'  => $user->device_token,
                'register_date' => dateFormat($user->created_at, 'full'),
            ]
        ]);
    }

    public function update(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'name'  => 'required',
	        'email' => 'required|email|unique:users,email,' . $user->id,
            'phone' => 'required|numeric|digits_between:10,14',
	    ]);

	    $user->update([
	    	'name' 	=> $request->name,
	    	'email' => $request->email,
	    	'phone' => phoneFormat($request->phone)
	    ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Update profile success.'],
            'data'     => [
                'name'          => $user->name,
                'email'         => $user->email,
                'phone'         => $user->phone,
                'device_token'  => $user->device_token,
                'register_date' => dateFormat($user->created_at, 'full'),
            ]
        ]);
    }

    public function changePassword(Request $request)
    {
    	$request->validate([
	        'password' 	   => 'required|min:6',
	        'new_password' => 'required|min:6|confirmed'
	    ]);

    	$user = auth()->user();
	    if (!Hash::check($request->password, $user->password)) {
    		return response()->json([
                'messages' => ['Password is invalid.']
            ], 400);
    	}

	    $user->update([
	    	'password' => bcrypt($request->new_password)
	    ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Change password success.']
        ]);
    }

    public function deviceToken(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'device_token' => 'required|string',
	    ]);

	    $user->update([
	    	'device_token' => $request->device_token,
	    ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Update device token success.']
        ]);
    }
}
