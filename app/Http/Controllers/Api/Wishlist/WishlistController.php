<?php

namespace App\Http\Controllers\Api\Wishlist;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function index()
    {
    	$user = auth()->user();

    	$products = $user->wishlists()->paginate(10);

    	$data = [];
        foreach ($products as $key => $product) {
            $data[] = [
                'id'       => $product->id,
                'name'     => $product->name,
                'image'    => storageUrl($product->images[0]),
                'price'    => $product->price,
                'category' => [
                	'id'   => $product->category->id,
                	'name' => $product->category->name,
                ],
            ];
        }

    	return response()->json([
            'status' => 'success',
            'data'   => $data,
            'links'  => [
                'current_page'  => $products->currentPage(),
                'next_page_url' => $products->nextPageUrl(),
            ]
        ]);
    }

    public function add(Request $request)
    {
    	$request->validate([
    		'product_id' => 'required'
    	]);

    	$user = auth()->user();

    	$user->wishlists()->syncWithoutDetaching([$request->product_id]);

    	return response()->json([
            'status'   => 'success',
            'messages' => ['Successfully add product to wishlist.']
        ]);
    }

    public function delete(Request $request)
    {
    	$request->validate([
    		'product_id' => 'required'
    	]);

    	$user = auth()->user();

    	$user->wishlists()->detach($request->product_id);

    	return response()->json([
            'status'   => 'success',
            'messages' => ['Successfully remove product from wishlist.']
        ]);
    }
}
