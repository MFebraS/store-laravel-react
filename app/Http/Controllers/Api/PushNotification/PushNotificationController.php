<?php

namespace App\Http\Controllers\Api\PushNotification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\ApiHelper;

class PushNotificationController extends Controller
{
    public function index()
    {
    	$firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();

        $data = [
            'to' => $firebaseToken,
            'notification' => [
                'title' => 'Title',
                'body' => 'Hello World',  
            ]
        ];

        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . env('FIREBASE_SERVER_KEY'),
        ];

        $url = 'https://fcm.googleapis.com/fcm/send';
        $response = ApiHelper::post($url, $data, $headers);

        dd($response);
    }

    public function send($token, $notification, $data=null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = [
            'Authorization: key=' . env('FIREBASE_SERVER_KEY'),
        ];

        $params = [
            'to' => $token,
            'notification' => $notification,
        ];
        if (!empty($data)) {
        	$params['data'] = $data;
        }

        ApiHelper::post($url, $params, $headers);
    }
}
