<?php

namespace App\Http\Controllers\Api\Cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Courier;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Helpers\OrderHelper;

class CartController extends Controller
{
    public function index()
    {
    	$user = auth()->user();
    	$carts = Cart::with('product')->where('user_id', $user->id)->latest()->get();

    	$data = [];
    	foreach ($carts as $key => $cart) {
    		$data[] = [
    			'id' 	   => $cart->id,
    			'quantity' => $cart->quantity,
    			'product'  => [
    				'id' 	=> $cart->product->id,
    				'name'  => $cart->product->name,
    				'image' => storageUrl($cart->product->images[0]),
    				'price' => $cart->product->price,
    			]
    		];
    	}

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    public function add(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'product_id' => 'required|exists:products,id',
	        'quantity' 	 => 'required|integer'
	    ]);

	    Cart::updateOrCreate(
	    	[
		    	'user_id'  	 => $user->id,
	    		'product_id' => $request->product_id
	    	],
	    	[
		    	'quantity' 	 => $request->quantity,
		    ]
		);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Product has been added to cart.']
        ]);
    }

    public function update(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'id' 	   => 'required|exists:carts,id',
	        'quantity' => 'required|integer'
	    ]);

	    $cart = Cart::where('user_id', $user->id)->findOrFail($request->id);
	    $cart->update([
	    	'quantity' => $request->quantity
	    ]);

        return response()->json([
            'status' => 'success',
            'data'   => $cart
        ]);
    }

    public function delete(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'id' => 'required|exists:carts,id',
	    ]);

	    $cart = Cart::where('user_id', $user->id)->findOrFail($request->id);
	    $cart->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Product has been removed from cart.']
        ]);
    }

    public function checkout(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'address_id' 	  => 'required|exists:addresses,id',
	        'products' 		  => 'required|array',
	        'courier_code' 	  => 'required',
	        'courier_service' => 'required'
	    ]);

    	$subtotal_price = 0;
    	$weight = 0;
    	$products = [];
	    foreach ($request->products as $key => $item) {
	    	$product = Product::findOrFail($item['id']);
            if ($item['quantity'] > $product->stock) {
                return response()->json([
                    'messages' => ['Insufficient stock.', 'Current stock for '. $product->name . ' : ' . numberFormat($product->stock, '')]
                ], 400);
            }
	    	$products[] = [
	    		'id' 	   => $product->id,
	    		'code' 	   => $product->code,
	    		'name' 	   => $product->name,
	    		'image'    => storageUrl($product->images[0]),
	    		'price'    => numberFormat($product->price),
	    		'quantity' => $item['quantity'],
	    		'note' 	   => $item['note'],
	    	];
	    	$subtotal_price += $product->price * $item['quantity'];
    		$weight += $product->weight * $item['quantity'];
	    }

	    $address = Address::with('city')->where('user_id', $user->id)->find($request->address_id);
	    $result = OrderHelper::getShippingFee($address->city_id, $weight, $request->courier_code, $request->courier_service);
	    if (isset($result['messages'])) {
		    return response()->json([
		    	'messages' => $result['messages']
		    ], 400);
	    }

		$shipping_fee = $result['cost']['value'];
	    $total_price = $subtotal_price + $shipping_fee;

	    $data = [
	    	'products' => $products,
	    	'shipping' => [
	    		'address' => [
	    			'id' 	   => $address->id,
	    			'label'    => $address->label,
	    			'address'  => $address->address,
	    			'city' 	   => $address->city->city_name,
	    			'province' => $address->city->province,
	    			'note' 	   => $address->note,
	    		],
	    		'phone' => $user->phone,
	    		'email' => $user->email,
	    	],
	    	'subtotal_price' => numberFormat($subtotal_price),
	    	'shipping_fee' 	 => numberFormat($shipping_fee),
	    	'total_price' 	 => numberFormat($total_price),
	    ];

	    return response()->json([
	    	'status' => 'success',
	    	'data' 	 => $data
	    ]);
    }

    public function shippingFee(Request $request)
    {
    	$user = auth()->user();
    	$request->validate([
	        'address_id'   	  => 'required|exists:addresses,id',
	        'products' 	   	  => 'required|array',
	        'courier_code' 	  => 'sometimes',
	        'courier_service' => 'sometimes'
	    ]);

    	$weight = 0;
	    foreach ($request->products as $key => $item) {
	    	$product = Product::findOrFail($item['id']);
    		$weight += $product->weight * $item['quantity'];
	    }

	    $address = Address::where('user_id', $user->id)->find($request->address_id);
	    if (isset($request->courier_code)) {
	    	// get 1 shipping fee
	    	$result = OrderHelper::getShippingFee($address->city_id, $weight, $request->courier_code, $request->courier_service);
		    
		    if (isset($result['messages'])) {
			    return response()->json([
			    	'messages' => $result['messages']
			    ], 400);
		    }
	    	$shipping_fee = $result;
	    }
	    else {
	    	// get all shipping fees
	    	$couriers = Courier::get();
	    	$shipping_fee = [];
	    	foreach ($couriers as $key => $courier) {
	    		$result = OrderHelper::getShippingFee($address->city_id, $weight, $courier->code);
			    if (!isset($result['messages'])) {
	    			$shipping_fee[] = $result;
		    	}
	    	}
	    }

	    return response()->json([
	    	'status' => 'success',
	    	'data'	 => $shipping_fee
	    ]);
    }

    public function paymentMethod()
    {
    	$payment_methods = PaymentMethod::where('active', true)->get()->groupBy('type');

    	$data = [];
    	foreach ($payment_methods as $key => $payment_types) {
    		$temp['type'] = $key;
    		$temp['name'] = ucwords(str_replace('_', ' ', $key));
    		foreach ($payment_types as $i => $item) {
    			$temp['payment_channels'][] = [
    				'id' 	  => $item->id,
    				'channel' => $item->channel,
    				'image'   => storageUrl($item->image),
    			];
    		}
    		$data[] = $temp;
    	}

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }
}
