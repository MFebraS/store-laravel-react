<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Helpers\OrderHelper;
use Log;

class MidtransNotificationController extends Controller
{
    /**
     * Handle Midtrans notification/ webhook
     * docs: https://docs.midtrans.com/en/after-payment/http-notification
     */
    public function index(Request $request)
    {
        $response = OrderHelper::orderStatus($request->transaction_id);

        $order = Order::with('user')->where('code', $response['order_id'])->firstOrFail();

        $order_payment = OrderPayment::where('order_id', $order->id)
        	->where('transaction_id', $response['transaction_id'])
        	->firstOrFail();

        try {
            $status = $response['transaction_status'];
            // update order status
            if ($status == 'capture') {
                if ($response['payment_type'] == 'credit_card') {
                    if ($response['fraud_status'] == 'challenge') {
                        // TODO set payment status in merchant's database to 'Challenge by FDS'
                        // TODO merchant should decide whether this order is authorized or not in MAP
                        $order_payment->note  = 'Challenge by FDS';
                    }
                    else {
                        $order->status = 'Paid';
                        $notification = [
                            'title' => 'Payment Settlement',
                            'body'  => 'We will process your order immediately.'
                        ];
                    }
                }
            }
            elseif ($status == 'settlement') {
                $order->status = 'Paid';
                $notification = [
                    'title' => 'Payment Settlement',
                    'body'  => 'We will process your order immediately.'
                ];
            }
            else if ($status == 'deny') {
                $order_payment->note = '(Deny) Midtrans Fraud Detection System reject';
            }
            else if ($status == 'cancel') {
                $order->status = 'Canceled';
                $order_payment->note  = 'Canceled by Midtrans or Merchant';
            }
            else if ($status == 'expire') {
                $order->status = 'Expired';
                $order_payment->note  = 'Midtrans payment expired';
            }
            else if ($status == 'refund') {
                $order->status = 'Canceled';
                $order_payment->note  = 'Refund order';
            }
            
            $order->save();
            
            $order_payment->status = $status;
            $order_payment->fraud_status = $response['fraud_status'] ?? null;
            $order_payment->save();

        } catch (Exception $e) {
            Log::channel('midtrans')->info('===============================');
            Log::channel('midtrans')->error('Error : ' . json_encode($e->getMessage()));
            Log::channel('midtrans')->info('Response : ' . json_encode($response));
            Log::channel('midtrans')->info('#' . "\n");
        }

        // TODO: send email to customer
        if (isset($notification)) {
            PushNotificationHelper::send($order->user->device_token, $notification);
        }

        return response()->json([
        	'status' => 'success'
        ]);
    }
}
