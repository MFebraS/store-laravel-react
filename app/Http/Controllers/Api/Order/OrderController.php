<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Courier;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderPayment;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Helpers\OrderHelper;
use App\Helpers\PushNotificationHelper;
use DB;

class OrderController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $orders = Order::with('order_details.product')->where('user_id', $user->id)->paginate(15);

        $data = [];
        foreach ($orders as $key => $order) {
            $order_details = $order->order_details->take(3);
            $images = [];
            foreach ($order_details as $key => $order_detail) {
                $images[] = storageUrl($order_detail->product->images[0]);
            }
            $product_name = null;
            if ($order_details->count() == 1) {
                $product_name = $order_details->first()->product_name;
            }
            $data[] = [
                'id'             => $order->id,
                'product_name'   => $product_name,
                'product_images' => $images,
                'total_price'    => numberFormat($order->total_price),
                'status'         => $order->status,
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
            'links'  => [
                'current_page'  => $orders->currentPage(),
                'next_page_url' => $orders->nextPageUrl(),
            ]
        ]);
    }

    public function create(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'address_id'        => 'required|exists:addresses,id',
            'payment_method_id' => 'required|exists:payment_methods,id',
            'products'          => 'required|array',
            'courier_code'      => 'required',
            'courier_service'   => 'required',
        ]);

        $courier = Courier::where('code', $request->courier_code)->firstOrFail();
        $payment_method = PaymentMethod::findOrFail($request->payment_method_id);

        // generate order code
        do {
            $code = mt_rand(10000000, 99999999);    // random 8 digits number
            $check_code = Order::where('code', $code)->first();
        } while (!empty($check_code));

        DB::beginTransaction();
            $subtotal_price = 0;
            $weight = 0;
            $order_details = [];
            foreach ($request->products as $key => $item) {
                $product = Product::findOrFail($item['id']);
                if ($item['quantity'] > $product->stock) {
                    return response()->json([
                        'messages' => ['Insufficient stock.', 'Current stock for '. $product->name . ' : ' . numberFormat($product->stock, '')]
                    ], 400);
                }
                $temp_subtotal_price = $product->price * $item['quantity'];
                $order_detail = [
                    'product_id'     => $product->id,
                    'product_code'   => $product->code,
                    'product_name'   => $product->name,
                    'price'          => $product->price,
                    'quantity'       => $item['quantity'],
                    'subtotal_price' => $temp_subtotal_price,
                    'note'           => $item['note']
                ];
                $product->update([
                    'stock' => ($product->stock - $item['quantity'] >= 0) ?
                                $product->stock - $item['quantity'] : 0
                ]);

                $subtotal_price += $temp_subtotal_price;
                $weight += $product->weight * $item['quantity'];
                $order_details[] = $order_detail;
            }

            $address = Address::with('city')->where('user_id', $user->id)->find($request->address_id);
            $result = OrderHelper::getShippingFee($address->city_id, $weight, $request->courier_code, $request->courier_service);
            if (isset($result['messages'])) {
                return response()->json([
                    'messages' => $result['messages']
                ], 400);
            }

            $shipping_fee = $result['cost']['value'];
            $total_price = $subtotal_price + $shipping_fee;

            $min_day = null;
            $max_day = null;
            if (isset($result['cost']['etd'])) {
                $shipping_day = explode('-', $result['cost']['etd']);
                $min_day = $shipping_day[0];
                $max_day = $shipping_day[1];
            }

            $order = Order::create([
                'user_id'         => $user->id,
                'phone'           => $user->phone,
                'code'            => $code,
                'subtotal_price'  => $subtotal_price,
                'shipping_fee'    => $shipping_fee,
                'total_price'     => $total_price,
                'status'          => 'Pending',
                'city_id'         => $address->city_id,
                'name'            => $address->name,
                'phone'           => $address->phone,
                'address'         => $address->address,
                'point'           => $address->point,
                'address_note'    => $address->note,
                'courier'         => $courier->name,
                'courier_service' => $request->courier_service
                'min_day'         => $min_day,
                'max_day'         => $max_day,
            ]);

            foreach ($order_details as $key => $order_detail) {
                $order_detail['order_id'] = $order->id;
                OrderDetail::create($order_detail);
            }

            // midtrans
            $midtrans = OrderHelper::chargeOrder($payment_method, $order->code, $order->total_price);

            if ($midtrans === false) {
                return response()->json([
                    'messages' => ['Failed to create payment.']
                ], 400);
            }

            $order_payment = OrderPayment::create([
                'order_id'         => $order->id,
                'transaction_id'   => $midtrans['transaction_id'],
                'va_number'        => $midtrans['va_numbers'][0]['va_number'],
                'payment_channel'  => $midtrans['va_numbers'][0]['bank'],
                'payment_type'     => $midtrans['payment_type'],
                'status'           => $midtrans['transaction_status'],
                'fraud_status'     => $midtrans['fraud_status'],
                'transaction_time' => $midtrans['transaction_time'],
                'expiry_time'      => $midtrans['expiry_time'] ?? null,
            ]);
        DB::commit();

        $notification = [
            'title' => 'Order Created Successfully',
            'body'  => 'Please make payment before ' . date('d F Y H:i', strtotime($midtrans['expiry_time']))
        ];
        PushNotificationHelper::send($user->device_token, $notification);
        
        // TODO: send invoice email to user

        return response()->json([
            'status' => 'success',
            'data'   => [
                'order_id'        => $order->id,
                'order_code'      => $order->code,
                'va_number'       => $midtrans['va_numbers'][0]['va_number'],
                'payment_channel' => $payment_method->channel,
                'payment_type'    => ucwords(str_replace('_', ' ', $midtrans['payment_type'])),
                'expiry_time'     => $midtrans['expiry_time'] ?? null,
                'amount'          => $total_price
            ]
        ]);
    }

    public function detail($id)
    {
        $user = auth()->user();
        $order = Order::with('city', 'order_details.product:id,images', 'order_payment')->where('user_id', $user->id)->findOrFail($id);

        $order_details = [];
        foreach ($order->order_details as $key => $order_detail) {
            $order_details[] = [
                'product_name'   => $order_detail->product_name,
                'image'          => storageUrl($order_detail->product->images[0]),
                'quantity'       => $order_detail->quantity,
                'price'          => $order_detail->price,
                'subtotal_price' => $order_detail->subtotal_price,
                'note'           => $order_detail->note,
            ];
        }

        $data = [
            'code'            => $order->code,
            'subtotal_price'  => $order->subtotal_price,
            'shipping_fee'    => $order->shipping_fee,
            'total_price'     => $order->total_price,
            'status'          => $order->status,
            'cancel_reason'   => $order->cancel_reason,
            'date'            => dateFormat($order->created_at, 'full'),
            'shipment'        => [
                'name'            => $order->name,
                'phone'           => $order->phone,
                'address'         => $order->address,
                'city'            => $order->city->city_name,
                'province'        => $order->city->province,
                'address_note'    => $order->address_note,
                'courier'         => $order->courier,
                'courier_service' => $order->courier_service,
                'min_day'         => $order->min_day,
                'max_day'         => $order->max_day,
            ],
            'order_details' => $order_details,
            'order_payment' => [
                'va_number'       => $order->order_payment->status == 'pending' ?
                                        $order->order_payment->va_number : null,
                'payment_type'    => ucwords(str_replace('_', ' ', $order->order_payment->payment_type)),
                'payment_channel' => strtoupper($order->order_payment->payment_channel),
                'status'          => ucwords($order->order_payment->status),
                'expiry_time'     => date('d F Y H:i', strtotime($order->order_payment->expiry_time)),
                'note'            => $order->order_payment->note,
            ],
        ];

        return response()->json([
            'status' => 'success',
            'data'   => $data,
        ]);
    }
}
