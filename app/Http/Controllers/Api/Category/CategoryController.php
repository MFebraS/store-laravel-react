<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('name')->get();

        $data = [];
        foreach ($categories as $key => $category) {
            $data[] = [
                'id'   => $category->id,
                'name' => $category->name
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }
}
