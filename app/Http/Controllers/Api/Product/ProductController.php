<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $category_id = $request->category_id;
        $user = auth('sanctum')->user();
        if ($user) {
            $wishlists = $user->wishlists->pluck('id')->all();
        }
        else {
            $wishlists = [];
        }

        $products = Product::with('category')
        	->where('status', 'Show')
        	->when($category_id, function($q) use ($category_id) {
                $q->where('category_id', $category_id);
            })
        	->when($search, function($q) use ($search) {
                $q->where(function($q) use ($search) {
                    $q->where('name', 'ilike', '%'.$search.'%')
                        ->orWhere('description', 'ilike', '%'.$search.'%');
                });
            })
            ->latest()
            ->paginate(10);

        $data = [];
        foreach ($products as $key => $product) {
            $data[] = [
                'id'       => $product->id,
                'name'     => $product->name,
                'image'    => storageUrl($product->images[0]),
                'price'    => $product->price,
                'wishlist' => in_array($product->id, $wishlists),
                'category' => [
                	'id'   => $product->category->id,
                	'name' => $product->category->name,
                ],
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
            'links'  => [
                'current_page'  => $products->currentPage(),
                'next_page_url' => $products->nextPageUrl(),
            ]
        ]);
    }

    public function detail($id)
    {
    	$product = Product::where('status', 'Show')->findOrFail($id);

        $user = auth('sanctum')->user();
        if ($user) {
            $wishlist = $user->wishlists()->where('product_id', $product->id)->first();
            $cart = $user->carts()->where('product_id', $product->id)->first();
        }

    	$images = [];
    	foreach ($product->images as $key => $image) {
    		$images[] = storageUrl($image);
    	}
    	$data = [
            'id'          => $product->id,
            'code'        => $product->code,
            'name'        => $product->name,
            'description' => $product->description,
            'images'      => $images,
            'price'       => $product->price,
            'stock'       => $product->stock,
            'weight'      => $product->weight,
            'weight_unit' => 'g',
            'cart'        => isset($cart),
            'wishlist'    => isset($wishlist),
            'category' 	  => [
            	'id'   => $product->category->id,
            	'name' => $product->category->name,
            ],
        ];

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Product recommendation in detail product
     */
    public function recommendation($id)
    {
        $product = Product::findOrFail($id);

        $products = Product::where('category_id', $product->category_id)->where('id', '!=', $id)->inRandomOrder()->take(10)->get();

        $user = auth('sanctum')->user();
        if ($user) {
            $wishlists = $user->wishlists->pluck('id')->all();
        }
        else {
            $wishlists = [];
        }

        $data = [];
        foreach ($products as $key => $product) {
            $data[] = [
                'id'       => $product->id,
                'name'     => $product->name,
                'image'    => storageUrl($product->images[0]),
                'price'    => $product->price,
                'wishlist' => in_array($product->id, $wishlists),
                'category' => [
                    'id'   => $product->category->id,
                    'name' => $product->category->name,
                ],
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
        ]);
    }
}
