<?php

namespace App\Http\Controllers\Api\Address;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $addresses = Address::with('city')->where('user_id', $user->id)->get();

        $data = [];
        foreach ($addresses as $key => $address) {
        	$data[] = [
        		'id' 	  => $address->id,
        		'label'   => $address->label,
        		'address' => $address->address .', '. $address->city->type .' '. $address->city->city_name,
                'city_id' => $address->city_id,
        		'primary' => $address->primary,
        	];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    public function primary()
    {
        $user = auth()->user();
    	$address = Address::where('user_id', $user->id)->where('primary', 1)->first();

        return response()->json([
            'status' => 'success',
            'data'   => $address
        ]);
    }

    public function detail($id)
    {
        $user = auth()->user();
        $address = Address::where('user_id', $user->id)->findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $address
        ]);
    }

    public function add(Request $request)
    {
    	$request->validate([
    		'city_id' 	=> 'required|exists:cities,id',
            'label'     => 'required',
            'name'      => 'required',
    		'phone' 	=> 'required|numeric|digits_between:10,14',
    		'address' 	=> 'required',
    		'latitude' 	=> 'required|numeric',
    		'longitude' => 'required|numeric',
    		'primary' 	=> 'nullable|boolean',
    	]);

        $user = auth()->user();
    	$address = Address::create([
	        'user_id'    => $user->id,
	        'city_id'    => $request->city_id,
            'label'      => $request->label,
            'name'       => $request->name,
	        'phone'      => phoneFormat($request->phone),
	        'address'    => $request->address,
            'coordinate' => "$request->latitude, $request->longitude",
	        'primary'    => $request->primary ?? 0,
	        'note'       => $request->note ?? null
    	]);

    	// change primary address
    	if ($request->primary == 1) {
    		Address::where('user_id', $user->id)
                ->where('primary', 1)
                ->where('id', '!=', $address->id)
                ->update([
                    'primary' => 0
                ]);
    	}

        return response()->json([
            'status'   => 'success',
            'messages' => ['Add address success.']
        ]);
    }

    public function update(Request $request)
    {
    	$request->validate([
    		'id' 		=> 'required',
    		'city_id' 	=> 'required|exists:cities,id',
    		'label' 	=> 'required',
            'name'      => 'required',
            'phone'     => 'required|numeric|digits_between:10,14',
    		'address' 	=> 'required',
    		'latitude' 	=> 'required|numeric',
    		'longitude' => 'required|numeric',
    		'primary' 	=> 'nullable|boolean',
    	]);

        $user = auth()->user();
    	$address = Address::where('user_id', $user->id)->findOrFail($request->id);
    	$address->update([
	        'city_id'    => $request->city_id,
	        'label'      => $request->label,
            'name'       => $request->name,
            'phone'      => phoneFormat($request->phone),
	        'address'    => $request->address,
            'coordinate' => "$request->latitude, $request->longitude",
	        'primary'    => $request->primary ?? 0,
	        'note' 	     => $request->note ?? null
    	]);

    	// change primary address
    	if ($request->primary == 1) {
    		Address::where('user_id', $user->id)
                ->where('primary', 1)
                ->where('id', '!=', $address->id)
                ->update([
        			'primary' => 0
        		]);
    	}

        return response()->json([
            'status'   => 'success',
            'messages' => ['Update address success.']
        ]);
    }

    public function delete(Request $request)
    {
    	$request->validate([
    		'id' => 'required',
    	]);

        $user = auth()->user();
    	$address = Address::where('user_id', $user->id)->findOrFail($request->id);
    	$address->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete address success.']
        ]);
    }
}
