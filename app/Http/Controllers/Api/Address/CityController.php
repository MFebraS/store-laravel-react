<?php

namespace App\Http\Controllers\Api\Address;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;

class CityController extends Controller
{
    public function index(Request $request)
    {
    	$cities = City::when($request->search, function($q) use($request) {
	    		$q->where('city_name', 'like', "%$request->search%");
	    	})
	    	->orderBy('province_id')
	    	->get();
        
        return response()->json([
            'status' => 'success',
            'data'   => $cities
        ]);
    }
}
