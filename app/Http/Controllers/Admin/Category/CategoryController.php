<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('name')->get();
        
        $data = [];
        foreach ($categories as $key => $category) {
            $data[] = [
                'id'         => $category->id,
                'name'       => $category->name,
                'created_at' => dateFormat($category->created_at)
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:categories,name',
        ]);

        Category::create([
            'name' => $request->name
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Create category success.']
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id'   => 'required',
            'name' => 'required|string|unique:categories,name,' . $request->id,
        ]);

        $category = Category::findOrFail($request->id);
        $category->update([
            'name' => $request->name
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Edit category success.']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $category = Category::withCount('products')->findOrFail($request->id);
        if ($category->products_count > 0) {
            return response()->json([
                'messages' => ['Can not delete category.', 'Please remove all products with '. $category->name .' category.'],
            ], 400);
        }

        $category->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete category success.'],
        ]);
    }
}
