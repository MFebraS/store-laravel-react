<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use DB;
use Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->query('search');
        $products = Product::with('category')
            ->when($keyword, function($q) use ($keyword) {
                $q->where(function($q) use ($keyword) {
                    $q->where('name', 'ilike', '%'.$keyword.'%')
                        ->orWhere('code', 'ilike', '%'.$keyword.'%')
                        ->orWhere('description', 'ilike', '%'.$keyword.'%');
                });
            })
            ->latest()
            ->paginate(10);

        $data = [];
        foreach ($products as $key => $product) {
            $data[] = [
                'id'         => $product->id,
                'code'       => $product->code,
                'name'       => $product->name,
                'category'   => $product->category,
                'image'      => storageUrl($product->images[0]),
                'price'      => numberFormat($product->price),
                'stock'      => numberFormat($product->stock, ''),
                'status'     => $product->status,
                'created_at' => dateFormat($product->created_at)
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
            'links'  => [
                'total'         => $products->total(),
                'current_page'  => $products->currentPage(),
                'first_item'    => $products->firstItem(),
                'last_item'     => $products->lastItem(),
                'prev_page_url' => $products->previousPageUrl(),
                'next_page_url' => $products->nextPageUrl(),
                'last_entry'    => $products->firstItem() - 1
            ]
        ]);
    }

    /**
     * Get category options
     */
    public function category()
    {
        $categories = Category::orderBy('name')->get();
        $options = [];
        foreach ($categories as $key => $category) {
            $options[] = [
                'value' => $category->id,
                'label' => $category->name
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $options
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'name'        => 'required',
            'price'       => 'required|integer|min:1',
            'weight'      => 'required|integer|min:1',
            'stock'       => 'required|integer|min:0',
            'images'      => 'required|array',
            'status'      => 'required|in:Show,Hidden',
            'code'        => 'nullable|string',
            'description' => 'nullable',
        ]);

        DB::beginTransaction();
            $product = Product::create([
                'category_id' => $request->category_id,
                'name'        => $request->name,
                'price'       => $request->price,
                'weight'      => $request->weight,
                'stock'       => $request->stock,
                'status'      => $request->status,
                'code'        => $request->code ?? null,
                'description' => $request->description ?? null,
            ]);

            // upload images
            $images = [];
            foreach ($request->images as $key => $image) {
                $upload_path = 'products/' . $product->id;
                $file_name   = $product->id .'_'. time() .'_'. $key;
                $file_path   = uploadFile($image, $upload_path, $file_name);
                $images[]    = $file_path;
            }
            $product->images = $images;
            $product->save();
        DB::commit();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Create product success.']
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('category')->findOrFail($id);
        $images = [];
        foreach ($product->images as $key => $image) {
            $images[] = storageUrl($image);
        }
        $product->images = $images;

        $categories = Category::orderBy('name')->get();
        $options = [];
        foreach ($categories as $key => $category) {
            $options[] = [
                'value' => $category->id,
                'label' => $category->name
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => [
                'product'    => $product,
                'categories' => $options,
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id'          => 'required',
            'category_id' => 'required|exists:categories,id',
            'name'        => 'required',
            'price'       => 'required|integer|min:1',
            'weight'      => 'required|integer|min:1',
            'stock'       => 'required|integer|min:0',
            'status'      => 'required|in:Show,Hidden',
            'images'      => 'required_without:old_images|array',
            'old_images'  => 'required_without:images|array',
            'code'        => 'nullable|string',
            'description' => 'nullable',
        ]);

        $product = Product::findOrFail($request->id);

        DB::beginTransaction();
            $product->update([
                'category_id' => $request->category_id,
                'name'        => $request->name,
                'price'       => $request->price,
                'weight'      => $request->weight,
                'stock'       => $request->stock,
                'status'      => $request->status,
                'code'        => $request->code ?? null,
                'description' => $request->description ?? null,
            ]);

            // upload images
            $images = [];
            if (isset($request->old_images)) {
                // convert full url to storage url
                foreach ($request->old_images as $old_image) {
                    $pos = strpos($old_image, "products");
                    $images[] = substr($old_image, $pos);
                }
            }
            // remove deleted images
            $removed_images = array_diff($product->images, $images);
            foreach ($removed_images as $key => $removed_image) {
                Storage::delete($removed_image);
            }

            if (isset($request->images)) {
                foreach ($request->images as $key => $image) {
                    $upload_path = 'products/' . $product->id;
                    $file_name   = $product->id .'_'. time() .'_'. $key;
                    $file_path   = uploadFile($image, $upload_path, $file_name);
                    $images[]    = $file_path;
                }
            }
            $product->images = $images;
            $product->save();
        DB::commit();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Edit product success.']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $product = Product::withCount('order_details')->findOrFail($request->id);
        if ($product->order_details_count > 0) {
            return response()->json([
                'messages' => ['Can not delete product.', 'Product already has order(s).'],
            ], 400);
        }

        Storage::deleteDirectory('products/' . $product->id);

        $product->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete product success.'],
        ]);
    }
}
