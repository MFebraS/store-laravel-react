<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::where('type', 'Admin')->orderBy('created_at')->get();
        
        $data = [];
        foreach ($users as $key => $user) {
            $data[] = [
                'id'         => $user->id,
                'name'       => $user->name,
                'email'      => $user->email,
                'status'     => $user->status,
                'created_at' => dateFormat($user->created_at)
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' 	   => 'required',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
        ]);

        User::create([
            'name' 	   => $request->name,
            'email'    => $request->email,
            'password' => bcrypt($request->password),
            'type'     => 'Admin',
            'status'   => 'Active',
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Create admin success.']
        ]);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $user
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' 	 	   => 'required',
            'name'  	   => 'required',
            'email' 	   => 'required|email|unique:users,email,' . $request->id,
            'password' 	   => 'required_with:new_password|min:6',
            'new_password' => 'required_with:password|min:6|confirmed',
        ]);

        $user = User::findOrFail($request->id);
        if (isset($request->password)) {
        	// check old password
        	if (Hash::check($request->password, $user->password)) {
		        $user->password = bcrypt($request->new_password);
        	}
        	else {
        		return response()->json([
	                'messages' => ['Password is invalid.']
	            ], 400);
        	}
        }

        $user->name  = $request->name;
        $user->email = $request->email;
        $user->save();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Edit admin success.']
        ]);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);
        if (auth()->guard('web')->id() === $request->id) {
    		return response()->json([
                'messages' => ['Can not delete current admin.', 'Please login with another account.']
            ], 400);
        }

        $user = User::findOrFail($request->id);
        $user->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete admin success.'],
        ]);
    }
}
