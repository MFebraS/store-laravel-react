<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
    	$keyword = $request->query('search');
        $users = User::where('type', 'Customer')
        	->when($keyword, function($q) use ($keyword) {
        		$q->where(function($q) use ($keyword) {
        			$q->where('name', 'ilike', '%'.$keyword.'%')
                        ->orWhere('email', 'ilike', '%'.$keyword.'%')
        				->orWhere('phone', 'like', '%'.$keyword.'%');
        		});
        	})
        	->latest()
        	->paginate(10);

        $data = [];
        foreach ($users as $key => $user) {
            $data[] = [
                'id'         => $user->id,
                'name'       => $user->name,
                'email'      => $user->email,
                'phone'      => $user->phone,
                'status'     => $user->status,
                'created_at' => dateFormat($user->created_at)
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
            'links'	 => [
            	'total'         => $users->total(),
            	'current_page'  => $users->currentPage(),
                'first_item'    => $users->firstItem(),
                'last_item'     => $users->lastItem(),
            	'prev_page_url' => $users->previousPageUrl(),
                'next_page_url' => $users->nextPageUrl(),
                'last_entry'    => $users->firstItem() - 1
            ]
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' 	   => 'required',
            'email'    => 'required|email|unique:users,email',
            'phone'    => 'required|numeric|digits_between:10,15',
            'password' => 'required|min:6|confirmed',
            'status'   => 'required',
        ]);

        User::create([
            'name' 	   => $request->name,
            'email'    => $request->email,
            'phone'    => $request->phone,
            'password' => bcrypt($request->password),
            'type'     => 'Customer',
            'status'   => $request->status,
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Create customer success.']
        ]);
    }

    public function detail($id)
    {
        $user = User::findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $user
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' 	 	   => 'required',
            'name'  	   => 'required',
            'email' 	   => 'required|email|unique:users,email,' . $request->id,
            'phone'        => 'required|numeric|digits_between:10,15',
            'password' 	   => 'required_with:new_password|min:6',
            'new_password' => 'required_with:password|min:6|confirmed',
            'status'  	   => 'required',
        ]);

        $user = User::findOrFail($request->id);
        if (isset($request->password)) {
        	// check old password
        	if (Hash::check($request->password, $user->password)) {
		        $user->password = bcrypt($request->new_password);
        	}
        	else {
        		return response()->json([
	                'messages' => ['Password is invalid.']
	            ], 400);
        	}
        }

        $user->name   = $request->name;
        $user->email  = $request->email;
        $user->phone  = $request->phone;
        $user->status = $request->status;
        $user->save();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Edit customer success.']
        ]);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $user = User::withCount(['carts', 'orders'])->findOrFail($request->id);

        if ($user->carts_count > 0) {
    		return response()->json([
                'messages' => ['Can not delete customer.', 'Customer already has cart(s).']
            ], 400);
        }
        if ($user->orders_count > 0) {
    		return response()->json([
                'messages' => ['Can not delete customer.', 'Customer already has order(s).']
            ], 400);
        }

        $user->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete customer success.'],
        ]);
    }
}
