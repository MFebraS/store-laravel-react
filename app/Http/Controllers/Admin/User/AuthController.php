<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'    => 'required|email',
            'password' => 'required|min:6'
        ]);

    	$credentials = $request->only('email', 'password');
        $user = User::where('email', $request->email)->where('type', 'Admin')->firstOrFail();

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json([
                'status'   => 'success',
                'messages' => ['Login success'],
                'data'     => [
                    'name'       => $user->name,
                    'email'      => $user->email,
                    'created_at' => $user->created_at->format('d F Y H:i:s'),
                ]
            ]);
        }

        return response()->json([
            'messages' => ['The provided credentials do not match our records.']
        ], 400);
    }

    public function logout()
    {
        Auth::guard('web')->logout();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Logout success']
        ]);
    }
}
