<?php

namespace App\Http\Controllers\Admin\Address;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Address;

class AddressController extends Controller
{
    public function index($user_id)
    {
        $addresses = Address::with('city')->where('user_id', $user_id)->get();

        $data = [];
        foreach ($addresses as $key => $address) {
            $data[] = [
                'id'        => $address->id,
                'city_id'   => $address->city_id,
                'label'     => $address->label,
                'address'   => $address->address,
                'latitude'  => $address->latitude,
                'longitude' => $address->longitude,
                'province'  => $address->city->province,
                'city'      => $address->city->city_name,
                'note'      => $address->note,
                'default'   => $address->default,
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => $data,
        ]);
    }

    public function cities()
    {
    	$cities = City::get();
    	$provinces = $cities->unique('province_id')->sortBy('province')->all();

    	$province_options = [];
        foreach ($provinces as $province) {
            $province_options[] = [
                'value' => $province->province_id,
                'label' => $province->province
            ];
        }

    	$city_options = [];
        foreach ($cities as $city) {
            $city_options[$city->province_id][] = [
                'value' => $city->id,
                'label' => $city->type .' '. $city->city_name
            ];
        }

        return response()->json([
            'status' => 'success',
            'data'   => [
            	'provinces' => $province_options,
            	'cities' => $city_options,
            ]
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id'   => 'required|exists:users,id',
            'label'     => 'required',
            'city_id'   => 'required|exists:cities,id',
            'address'   => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
        ]);

        if (isset($request->default) && $request->default === true) {
            Address::where('user_id', $request->user_id)->update([
                'default' => false
            ]);
        }

        Address::create([
            'user_id'   => $request->user_id,
            'label'     => $request->label,
            'city_id'   => $request->city_id,
            'address'   => $request->address,
            'latitude'  => $request->latitude,
            'longitude' => $request->longitude,
            'note'      => $request->note ?? null,
            'default'   => $request->default ?? false,
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Create address success.']
        ]);
    }

    public function edit($id)
    {
        $address = Address::with('user', 'city')->findOrFail($id);

        return response()->json([
            'status' => 'success',
            'data'   => $address,
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'id'        => 'required|exists:addresses,id',
            'label'     => 'required',
            'city_id'   => 'required|exists:cities,id',
            'address'   => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
        ]);

        $address = Address::findOrFail($request->id);

        if (isset($request->default) && $request->default === true) {
            Address::where('user_id', $address->user_id)->where('id', '!=', $request->id)->update([
                'default' => false
            ]);
        }

        $address->update([
            'label'     => $request->label,
            'city_id'   => $request->city_id,
            'address'   => $request->address,
            'latitude'  => $request->latitude,
            'longitude' => $request->longitude,
            'note'      => $request->note ?? null,
            'default'   => $request->default ?? false,
        ]);

        return response()->json([
            'status'   => 'success',
            'messages' => ['Update address success.']
        ]);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:addresses,id',
        ]);

        $address = Address::findOrFail($request->id)->delete();

        return response()->json([
            'status'   => 'success',
            'messages' => ['Delete address success.']
        ]);
    }
}
