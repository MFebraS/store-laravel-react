<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Order extends Model
{
    use HasFactory, Uuid;
    
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'user_id',
        'code',
        'subtotal_price',
        'shipping_fee',
        'total_price',
        'status',   // Pending, Paid, Process, Delivery, Delivered, Completed, Canceled, Expired
        'city_id',
        'name',
        'phone',
        'address',
        'point',
        'address_note',
        'courier',
        'courier_service',
        'cancel_reason'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function order_payment()
    {
        return $this->hasOne(OrderPayment::class);
    }
}
