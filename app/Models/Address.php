<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'city_id',
        'label',
        'name',
        'phone',
        'address',
        'coordinate',
        'primary',
        'note'
    ];

    public function setCoordinateAttribute($value)
    {
        $this->attributes['coordinate'] = DB::raw("POINT($value)");
    }

    public function getCoordinateAttribute($value)
    {
        $coordinate = unpack('x/x/x/x/corder/Ltype/dlat/dlon', $value);
        return $coordinate['lat'] . ', ' . $coordinate['lon'];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
