<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'order_id',
        'transaction_id',
        'va_number',
        'payment_type',
        'payment_channel',
        'status',
        'fraud_status',
        'note',
        'transaction_time',
        'expiry_time'
    ];
}
