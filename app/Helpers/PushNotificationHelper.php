<?php

namespace App\Helpers;

use App\Helpers\ApiHelper;

class PushNotificationHelper
{
	/**
	 * Firebase Push Notification
	 */
	public static function send($token, $notification, $data=null)
    {
    	if (!empty($token)) {
	        $url = 'https://fcm.googleapis.com/fcm/send';
	        $headers = [
	            'Authorization: key=' . env('FIREBASE_SERVER_KEY'),
	        ];

	        $params = [
	            'to' => $token,
	            'notification' => $notification,
	        ];
	        if (!empty($data)) {
	        	$params['data'] = $data;
	        }

	        ApiHelper::post($url, $params, $headers);
    	}
    }
}