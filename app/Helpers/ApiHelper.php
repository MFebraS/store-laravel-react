<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

class ApiHelper
{
	public static function get($url, $headers=[])
	{
		$client = new Client();
		$_headers = [
			'Accept' 	   => 'application/json',
			'Content-Type' => 'application/json'
		];
		$headers = array_merge($headers, $_headers);

		try {
			$response = $client->request('GET', $url, [
				'headers' => $headers
			]);
			$content = $response->getBody()->getContents();
		} catch (RequestException $e) {
			$content = $e->getResponse()->getBody()->getContents();
		} catch (ClientException $e) {
			$content = $e->getResponse()->getBody()->getContents();
		}

		return json_decode($content, true);
	}

	public static function post($url, $params, $headers=[])
	{
		$client = new Client();
		$_headers = [
			'Accept' 	   => 'application/json',
			'Content-Type' => 'application/json'
		];
		$headers = array_merge($headers, $_headers);

		try {
			$response = $client->request('POST', $url, [
				'headers' => $headers,
				'body' => json_encode($params)
			]);
			$content = $response->getBody()->getContents();
		} catch (RequestException $e) {
			$content = $e->getResponse()->getBody()->getContents();
		} catch (ClientException $e) {
			$content = $e->getResponse()->getBody()->getContents();
		}

		return json_decode($content, true);
	}
}