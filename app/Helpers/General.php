<?php

function storageUrl($path)
{
	if (empty($path)) {
		return null;
	}
	return Storage::url($path);
}

function numberFormat($number, $prefix='Rp', $sufix='')
{
	$number = number_format($number, 0, '', '.');
	return ($prefix ? $prefix . ' ' : '') . $number .($sufix ? ' ' . $sufix : '');
}

function dateFormat($date, $month='short')
{
	if (empty($date)) {
		return null;
	}
	
	$format = 'd M Y H:i:s';
	if ($month == 'full') {
		$format = 'd F Y H:i:s';
	}
	return date($format, strtotime($date));
}

function phoneFormat($phone)
{
    $phone = str_replace('+', '', $phone);
	return preg_replace('/^08/', '628', $phone);
}

function uploadFile($file, $path, $filename)
{
	if (is_string($file)) {
    	// get file extension
    	$file_extension = explode('/', mime_content_type($file))[1];
    	
		// decode base 64
    	$base64_index = strpos($file, 'base64,');
    	$file = base64_decode(substr($file, $base64_index + 7));
	}
	else {
		$file_extension = $file->getClientOriginalExtension();
		$file = file_get_contents($file);
	}
	
    $filename = $filename .'.'. $file_extension;

	// upload file
	Storage::put($path .'/'. $filename, $file);

	return $path .'/'. $filename;
}
