<?php

namespace App\Helpers;

use App\Models\Setting;
use App\Helpers\ApiHelper;

class OrderHelper
{
    public static function getShippingFee($destination_city_id, $weight, $courier_code, $courier_service=null)
    {
    	$setting = Setting::where('key', 'shipping_city_id')->first();
    	$origin_city_id = $setting->value;

    	$url = 'https://api.rajaongkir.com/starter/cost';
    	$headers = [
    		'key' => env('RAJA_ONGKIR_KEY')
    	];
    	$params = [
    		'origin' 	  => $origin_city_id,
    		'destination' => $destination_city_id,
    		'weight' 	  => $weight,
    		'courier' 	  => $courier_code,
    	];
    	$response = ApiHelper::post($url, $params, $headers);
    	if ($response['rajaongkir']['status']['code'] == 200) {
	    	$result = $response['rajaongkir']['results'][0];

	    	if (isset($courier_service)) {
	    		$search = false;
			    foreach ($result['costs'] as $key => $item) {
			    	if ($item['service'] == $courier_service) {
	    				$search = $item;
			    		break;
			    	}
			    }
			    if ($search === false) {
				    return [
				    	'messages' => ['Courier service not found.']
				    ];
			    }
		    	return [
		    		'courier_name' => $result['name'],
		    		'service' 	   => $search['service'],
		    		'description'  => $search['description'],
		    		'cost' 		   => $search['cost'][0],
		    	];
	    	}

	    	return [
	    		'courier_name' => $result['name'],
	    		'costs' 	   => $result['costs']
	    	];
    	}

    	return [
    		'messages' => $response['rajaongkir']['status']['description']
    	];
    }

	/**
	 * Midtrans Charge Order
	 */
	public static function chargeOrder($payment_method, $order_code, $order_price)
	{
		$url = env('MIDTRANS_URL') . '/charge';
		$headers = [
			'Authorization' => base64_encode(env('MIDTRANS_SERVER_KEY') . ':')
		];

		switch ($payment_method->type) {
			case 'bank_transfer':
				$params = [
					'payment_type' => $payment_method->type,
					'transaction_details' => [
						'order_id' => $order_code,
						'gross_amount' => (int) $order_price,
					],
					'bank_transfer' => [
						'bank' => $payment_method->code
					],
					'custom_expiry' => [
						'order_time' => date('Y-m-d H:i:s') . ' +0700',
						'expiry_duration' => 24,
						'unit'	=> 'hour'
					]
				];
				$expiry_time = date("Y-m-d H:i:s", strtotime('+24 hours'));
				break;
			
			default:
				return false;
				break;
		}

		$result = ApiHelper::post($url, $params, $headers);
		if (isset($expiry_time)) {
			$result['expiry_time'] = $expiry_time;
		}

		return $result;
	}

	/**
	 * Midtrans Order Status
	 */
	public static function orderStatus($transaction_id)
	{
		$url = env('MIDTRANS_URL') . '/' . $transaction_id . '/status';
		$headers = [
			'Authorization' => base64_encode(env('MIDTRANS_SERVER_KEY') . ':')
		];
		$result = ApiHelper::get($url, $headers);

		return $result;
	}
}