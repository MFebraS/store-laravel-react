<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Address\AddressController;
use App\Http\Controllers\Api\Address\CityController;
use App\Http\Controllers\Api\User\AuthController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\Category\CategoryController;
use App\Http\Controllers\Api\Product\ProductController;
use App\Http\Controllers\Api\Wishlist\WishlistController;
use App\Http\Controllers\Api\Cart\CartController;
use App\Http\Controllers\Api\Order\OrderController;
use App\Http\Controllers\Api\Order\MidtransNotificationController;
use App\Http\Controllers\Api\PushNotification\PushNotificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function() {
	Route::post('login', [AuthController::class, 'login']);
	Route::post('register', [AuthController::class, 'register']);

	Route::middleware('auth:sanctum')->group(function() {
		Route::get('logout', [AuthController::class, 'logout']);
	});
});

// Category
Route::prefix('category')->group(function() {
	Route::get('/', [CategoryController::class, 'index']);
});

// City
Route::post('city', [CityController::class, 'index']);

// Product
Route::prefix('product')->group(function() {
	Route::post('/', [ProductController::class, 'index']);
	Route::get('detail/{id}', [ProductController::class, 'detail']);
	Route::get('detail/recommendation/{id}', [ProductController::class, 'recommendation']);
});

Route::middleware('auth:sanctum')->group(function() {
	// User
	Route::prefix('user')->group(function() {
		Route::get('/', [UserController::class, 'index']);
		Route::post('update', [UserController::class, 'update']);
		Route::post('update/device-token', [UserController::class, 'deviceToken']);
		Route::post('change-password', [UserController::class, 'changePassword']);
	});

	// Address
	Route::prefix('address')->group(function() {
		Route::get('/', [AddressController::class, 'index']);
		Route::get('primary', [AddressController::class, 'primary']);
		Route::get('detail/{id}', [AddressController::class, 'detail']);
		Route::post('add', [AddressController::class, 'add']);
		Route::post('update', [AddressController::class, 'update']);
		Route::post('delete', [AddressController::class, 'delete']);
	});

	// Wishlist
	Route::prefix('wishlist')->group(function() {
		Route::get('/', [WishlistController::class, 'index']);
		Route::post('add', [WishlistController::class, 'add']);
		Route::post('delete', [WishlistController::class, 'delete']);
	});

	// Cart
	Route::prefix('cart')->group(function() {
		Route::get('/', [CartController::class, 'index']);
		Route::post('add', [CartController::class, 'add']);
		Route::post('update', [CartController::class, 'update']);
		Route::post('delete', [CartController::class, 'delete']);
		Route::post('checkout', [CartController::class, 'checkout']);
		Route::post('shipping-fee', [CartController::class, 'shippingFee']);
		Route::get('payment-method', [CartController::class, 'paymentMethod']);
	});

	// Order
	Route::prefix('order')->group(function() {
		Route::get('/', [OrderController::class, 'index']);
		Route::post('create', [OrderController::class, 'create']);
		Route::get('detail/{id}', [OrderController::class, 'detail']);
	});

});

// Notification
Route::prefix('notification')->group(function() {
	Route::post('midtrans', [MidtransNotificationController::class, 'index']);
});

// Push Notification
Route::prefix('push-notification')->group(function() {
	Route::get('/', [PushNotificationController::class, 'index']);
});