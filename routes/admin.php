<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\User\AdminController;
use App\Http\Controllers\Admin\User\AuthController;
use App\Http\Controllers\Admin\User\CustomerController;
use App\Http\Controllers\Admin\Address\AddressController;
use App\Http\Controllers\Admin\Category\CategoryController;
use App\Http\Controllers\Admin\Product\ProductController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::prefix('auth')->group(function() {
	Route::post('login', [AuthController::class, 'login']);

	Route::middleware('auth:sanctum')->group(function() {
		Route::get('logout', [AuthController::class, 'logout']);
	});
});

Route::middleware('auth:sanctum')->group(function() {
	// Admin
	Route::prefix('admin')->group(function() {
		Route::get('/', [AdminController::class, 'index']);
		Route::post('store', [AdminController::class, 'store']);
		Route::get('edit/{id}', [AdminController::class, 'edit']);
		Route::post('update', [AdminController::class, 'update']);
		Route::post('delete', [AdminController::class, 'delete']);
	});

	// Customer
	Route::prefix('customer')->group(function() {
		Route::get('/', [CustomerController::class, 'index']);
		Route::post('store', [CustomerController::class, 'store']);
		Route::get('detail/{id}', [CustomerController::class, 'detail']);
		Route::post('update', [CustomerController::class, 'update']);
		Route::post('delete', [CustomerController::class, 'delete']);
	});

	// Address
	Route::prefix('address')->group(function() {
		Route::get('/cities', [AddressController::class, 'cities']);
		Route::get('/{user_id}', [AddressController::class, 'index']);
		Route::post('store', [AddressController::class, 'store']);
		Route::get('edit/{id}', [AddressController::class, 'edit']);
		Route::post('update', [AddressController::class, 'update']);
		Route::post('delete', [AddressController::class, 'delete']);
	});

	// Category
	Route::prefix('category')->group(function() {
		Route::get('/', [CategoryController::class, 'index']);
		Route::post('store', [CategoryController::class, 'store']);
		Route::get('edit/{id}', [CategoryController::class, 'edit']);
		Route::post('update', [CategoryController::class, 'update']);
		Route::post('delete', [CategoryController::class, 'delete']);
	});

	// Product
	Route::prefix('product')->group(function() {
		Route::get('/', [ProductController::class, 'index']);
		Route::get('/category', [ProductController::class, 'category']);
		Route::post('store', [ProductController::class, 'store']);
		Route::get('edit/{id}', [ProductController::class, 'edit']);
		Route::post('update', [ProductController::class, 'update']);
		Route::post('delete', [ProductController::class, 'delete']);
	});
});
