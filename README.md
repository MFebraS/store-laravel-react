## About

Store is simple e-commerce build with Laravel v 8.12 and React Js v 16.14.0 with database Postgree SQL.

## Installation

- composer install
- sudo chmod 777 -R storage
- create database
- php artisan migrate
- php artisan db:seed
- npm install
- npm run watch

## Features

- Laravel :
	- Laravel sanctum
	- UUID (use Trait)
	- Custom route (admin)
	- Middleware API validation
	- Exception findOrFail and firstOrFail
- React Js :
	- React hooks
	- Helper (Request, Storage)
	- Redux
	- Map Leaflet
	- Components :
		- Alert
		- Collapse
		- Form
			- Image repeater
			- Input number
			- Select (with search)
		- Toast (slide in & out animation)
- Custom Admin Template