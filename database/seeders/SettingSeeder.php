<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
        	[
                'group' => 'shipping_address',
        		'key'   => 'shipping_address',
        		'value' => 'Jl. Taman Siswa no 22'
        	],
        	[
                'group' => 'shipping_address',
        		'key'   => 'shipping_city_id',
        		'value' => '501'
        	],
        	[
                'group' => 'shipping_address',
        		'key'   => 'shipping_phone',
        		'value' => '62811223344'
        	],
        ];

        foreach ($settings as $item) {
        	Setting::updateOrCreate(
                [ 'key' => $item['key'] ],
                [
                    'group' => $item['group'],
                    'value' => $item['value']
                ]
        	);
        }
    }
}
