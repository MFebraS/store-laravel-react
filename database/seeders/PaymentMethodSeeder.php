<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::truncate();
    	
        PaymentMethod::insert([
        	[
		        'type' 	  => 'bank_transfer',
		        'channel' => 'BNI',
		        'code' 	  => 'bni',
		        'image'	  => 'bni.png',
		        'active'  => 1
	        ],
        	[
		        'type' 	  => 'bank_transfer',
		        'channel' => 'BRI',
		        'code' 	  => 'bri',
		        'image'	  => 'bri.png',
		        'active'  => 1
	        ],
        	[
		        'type' 	  => 'bank_transfer',
		        'channel' => 'BCA',
		        'code' 	  => 'bca',
		        'image'	  => 'bca.png',
		        'active'  => 1
	        ],
        ]);
    }
}
