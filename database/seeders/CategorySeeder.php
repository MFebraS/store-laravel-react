<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Carbon\Carbon;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Category::truncate();

    	$categories = [
    		"Bed",
    		"Carpet",
            "Chair",
    		"Kitchen",
            "Shelf",
    		"Sofa",
    		"Table",
    		"Wardrobe",
    	];

    	$data = [];
    	foreach ($categories as $key => $value) {
    		$data[] = [
    			'name' => $value,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
    		];
    	}

        Category::insert($data);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
