<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Courier;

class CourierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Courier::truncate();

        Courier::insert([
        	[
	            'name'   => 'Jalur Nugraha Ekakurir (JNE)',
                'code'   => 'jne',
	            'image'  => 'jne.png',
                'active' => 1,
	        ],
        	[
	            'name'   => 'Citra Van Titipan Kilat (TIKI)',
	            'code'   => 'tiki',
                'image'  => 'tiki.png',
                'active' => 1,
	        ],
        	[
	            'name'   => 'POS Indonesia (POS)',
	            'code'   => 'pos',
                'image'  => 'pos.png',
                'active' => 1,
	        ],
        ]);
    }
}
