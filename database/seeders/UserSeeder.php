<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    	User::truncate();
        DB::table('personal_access_tokens')->truncate();
    	
        User::create([
            'name'     => 'Admin',
            'email'    => 'admin@mail.com',
            'password' => bcrypt('123456'),
            'type'     => 'Admin'
        ]);

        User::create([
            'name'     => 'Hello',
            'email'    => 'hello@mail.com',
            'phone'    => '628112233441',
            'password' => bcrypt('123456'),
            'type'     => 'Customer'
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
