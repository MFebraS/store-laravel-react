<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\City;
use DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        City::truncate();

        $data = file_get_contents('database/seeders/data/cities.json');
        $cities = json_decode($data);
        
        foreach ($cities as $key => $city) {
        	City::create([
        		'id'          => $city->city_id,
        		'province_id' => $city->province_id,
        		'province'    => $city->province,
        		'type'        => $city->type,
        		'city_name'   => $city->city_name,
        		'postal_code' => $city->postal_code
        	]);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
