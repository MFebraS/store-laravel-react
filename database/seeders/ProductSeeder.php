<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;
use DB;
use Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // remove images
        $storage_path = storage_path() . '/app/public/';
        $directories = Storage::allDirectories('products');
        foreach ($directories as $key => $directory) {
	        Storage::deleteDirectory($directory);
        }

        Cart::truncate();
        Product::truncate();

    	$faker = Faker\Factory::create();
    	$categories = Category::all();
    	$prices = [
    		1000000,
    		1500000,
    		2000000,
    		2300000,
    		2700000,
    		3000000,
    		3500000,
    		5000000,
    		5200000
    	];

    	$data = [];
		for ($i=0; $i < 100; $i++) {
            $category = $categories->random();
            $category_lc = strtolower($category->name);
			$code = strtoupper(Str::random(7)) . $i;
			
			$images = [$category_lc . '1.jpg', $category_lc . '2.jpg'];
			$new_images = [];
            foreach ($images as $key => $image) {
            	$image 		  = public_path('images/product-sample/' . $image);
                $upload_path  = 'products/' . $code . '/';
                $file_name    = $code .'_'. time() .'_'. $key . '.jpg';
                $file_path    = $upload_path . $file_name;
                $new_images[] = $file_path;

                if(!File::exists($storage_path . $upload_path)) {
				    File::makeDirectory($storage_path . $upload_path, 0777, true, true);
				}
                File::copy($image, $storage_path . $file_path);
            }

    		$data[] = [
    			'category_id' => $category->id,
		        'code' 		  => $code,
		        'name' 		  => ucwords($faker->words(3, true)),
		        'price' 	  => $faker->randomElement($prices),
		        'weight' 	  => $faker->numberBetween(100, 200),
		        'stock' 	  => $faker->numberBetween(10, 200),
		        'description' => $faker->paragraph(8, false),
		        'images' 	  => json_encode($new_images),
		        'status' 	  => 'Show',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
    		];
    	}

        Product::insert($data);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
