<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payments', function (Blueprint $table) {
            $table->id();
            $table->uuid('order_id');
            $table->string('transaction_id')->comment('midtrans transaction id');
            $table->string('va_number')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('status')->nullable();
            $table->string('fraud_status')->nullable();
            $table->string('note')->nullable();
            $table->timestamp('transaction_time')->nullable();
            $table->timestamp('expiry_time')->nullable();
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_payments');
    }
}
