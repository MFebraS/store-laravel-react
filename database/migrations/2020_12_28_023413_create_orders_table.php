<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->string('code')->unique()->comment('order code');
            $table->unsignedBigInteger('subtotal_price');
            $table->unsignedBigInteger('shipping_fee');
            $table->unsignedBigInteger('total_price');
            $table->enum('status', [
                    'Pending', 'Paid', 'Process', 'Delivery', 'Delivered', 'Completed', 'Canceled', 'Expired'
                ])
                ->default('Pending');
            $table->foreignId('city_id')
                ->constrained('cities')
                ->onUpdate('cascade')
                ->onDelete('no action');
            $table->string('name');
            $table->string('phone', 15);
            $table->string('address');
            $table->point('coordinate');
            $table->string('address_note')->nullable();
            $table->string('courier');
            $table->string('courier_service');
            $table->unsignedTinyInteger('min_day')->nullable();
            $table->unsignedTinyInteger('max_day')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
