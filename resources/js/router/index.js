import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Login } from '../views/admin/Auth';
import { Dashboard } from '../views/admin/Dashboard';
import { AdminList, AdminCreate, AdminEdit } from '../views/admin/Admin';
import { CustomerList, CustomerCreate, CustomerEdit, CustomerDetail } from '../views/admin/Customer';
import { AddressCreate, AddressEdit } from '../views/admin/Address';
import { CategoryList, CategoryCreate, CategoryEdit } from '../views/admin/Category';
import { ProductList, ProductCreate, ProductEdit } from '../views/admin/Product';

function Router(props) {
    const admin_route = process.env.MIX_BASE_ROUTE + '/back-office';

    return (
        <BrowserRouter basename={admin_route}>
        	<div className="admin">
	            <Switch>
                    <Route exact path="/" component={Login} />
                    <PrivateRoute exact path="/dashboard" component={Dashboard} />
                    
                    <PrivateRoute exact path="/admin" component={AdminList} />
                    <PrivateRoute exact path="/admin/create" component={AdminCreate} />
                    <PrivateRoute exact path="/admin/edit/:id" component={AdminEdit} />

                    <PrivateRoute exact path="/customer" component={CustomerList} />
                    <PrivateRoute exact path="/customer/create" component={CustomerCreate} />
                    <PrivateRoute exact path="/customer/edit/:id" component={CustomerEdit} />
                    <PrivateRoute exact path="/customer/detail/:id" component={CustomerDetail} />

                    <PrivateRoute exact path="/address/create" component={AddressCreate} />
                    <PrivateRoute exact path="/address/edit/:id" component={AddressEdit} />

                    <PrivateRoute exact path="/category" component={CategoryList} />
                    <PrivateRoute exact path="/category/create" component={CategoryCreate} />
	                <PrivateRoute exact path="/category/edit/:id" component={CategoryEdit} />

                    <PrivateRoute exact path="/product" component={ProductList} />
                    <PrivateRoute exact path="/product/create" component={ProductCreate} />
                    <PrivateRoute exact path="/product/edit/:id" component={ProductEdit} />
	            </Switch>
	        </div>
        </BrowserRouter>
    );
}

function PrivateRoute({ component, ...options }) {
    const user = useSelector(state => state.user);
    if (user) {
        return <Route {...options} component={component} />;
    }

    return <Redirect to="/" />;
};

export default Router;
