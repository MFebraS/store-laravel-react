import { getData, setData, clearData } from '../../helpers/Storage';

const STORE_USER = 'STORE_USER';
const CLEAR_USER = 'CLEAR_USER';

export function storeUser(data) {
	return {
		type: STORE_USER,
		data
	}
}

export function clearUser() {
	return {
		type: CLEAR_USER
	}
}

const defaultUser = getData('data') || null;

function user(state=defaultUser, action) {
	switch (action.type) {
		case STORE_USER:
			setData('data', action.data);
			return action.data;
			break;
		case CLEAR_USER:
			clearData('data');
			return null;
			break;
		default:
			return state;
			break;
	}
}

export default user;