import { createStore, combineReducers } from 'redux';
import loader from './loader/loader';
import user from './user/user';
import toast from './toast/toast';

const reducers = combineReducers({
    loader: loader,
    user: user,
    toast: toast
})
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;