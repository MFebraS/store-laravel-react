const SHOW_LOADER = 'SHOW_LOADER';
const HIDE_LOADER = 'HIDE_LOADER';

export function showLoader() {
	return {
		type: SHOW_LOADER
	}
}

export function hideLoader() {
	return {
		type: HIDE_LOADER
	}
}

const defaultLoader = 0;

function loader(state=defaultLoader, action) {
	switch (action.type) {
		case SHOW_LOADER:
			return state + 1;
			break;
		case HIDE_LOADER:
			return state - 1;
			break;
		default:
			return state;
			break;
	}
}

export default loader;