const STORE_TOAST = 'STORE_TOAST';
const CLEAR_TOAST = 'CLEAR_TOAST';

export function storeToast(data) {
	return {
		type: STORE_TOAST,
		data
	}
}

export function clearToast(data) {
	return {
		type: CLEAR_TOAST,
		data
	}
}

const defaultToast = {
	type: null,
	title: null,
	messages: null
};

function toast(state=defaultToast, action) {
	switch (action.type) {
		case STORE_TOAST:
			return {
				type: action.data.type,
				title: action.data.title,
				messages: action.data.messages
			};
			break;
		case CLEAR_TOAST:
			return {
				type: null,
				title: null,
				messages: null
			};
			break;
		default:
			return state;
			break;
	}
}

export default toast;