import axios from 'axios';
import { clearData } from './Storage';

const app_url = process.env.MIX_APP_URL;
const admin_url = process.env.MIX_ADMIN_URL;
const timeout = 5000;

export const get = async (url, base=false) => {
	url = (base ? app_url : admin_url) + url
	console.log('======= GET url :', url)

	try {
		const response = await axios({
			method	: 'get',
			url 	: url,
			timeout : timeout,
			headers : {
				'Accept': 'application/json',
			}
		})

		// request success
		console.log('======= GET response :', response.data)
		return response.data;

	} catch (e) {
		// request fail
		console.error('======= error :', e.response.data)
		if (e.response.status === 401 && e.response.data.message === 'Unauthenticated.') {
			clearData('data');
			return location.replace(app_url + 'back-office?p=unauthenticated');
		}
		return {
			status 	 : e.response.status || 'fail',
			messages : e.response.data.messages || [e.message]
		}
	}
}

export const post = async (url, params, base=false, type='json') => {
	url = (base ? app_url : admin_url) + url
	console.log('======= POST url :', url)
	console.log('======= POST params :', params)

	let headers = {
		'Accept': 'application/json'
	}

	if (type === 'form-data') {
		headers['Content-Type'] = 'multipart/form-data'

		// create form data
		let form_data = new FormData()
		Object.entries(params).forEach(entry => {
			let [key, value] = entry

			if (value) {
				if (Array.isArray(value)) {
					value.forEach( function(element, index) {
						form_data.append(`${key}[]`, element);
					})
				}
				else {
					form_data.append(key, value)
				}

				console.log('======= POST params form :', key, value)
			}
		})

		params = form_data
	}

	try {
		const response = await axios({
			method	: 'post',
			url 	: url,
			timeout : timeout,
			headers : headers,
			data 	: params
		})

		// request success
		console.log('======= POST response :', response.data)
		return response.data;

	} catch (e) {
		// request fail
		console.error('======= error :', e.response.data)
		if (e.response.status === 401 && e.response.data.message === 'Unauthenticated.') {
			clearData('data');
			return location.replace(app_url + 'back-office?p=unauthenticated');
		}
		return {
			status 	 : e.response.status || 'fail',
			messages : e.response.data.messages || [e.message]
		}
	}
}
