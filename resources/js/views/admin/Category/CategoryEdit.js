import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';

function CategoryEdit() {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();
    const path_variable = useParams();

    useEffect(() => {
        getCategory();
    }, []);

    useEffect(() => {
        if (id && name) {
            setDisable(false);
        }
        else {
            setDisable(true);
        }
    }, [id, name]);

    const getCategory = async () => {
        dispatch(showLoader());
        let response = await get('category/edit/' + path_variable.id);
        if (response) {
            if (response.data) {
                setId(response.data.id);
                setName(response.data.name);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages,
                }
                dispatch(storeToast(toast));
                history.push('/category');
            }
            dispatch(hideLoader());
        }
    }

    const updateCategory = async (e) => {
        e.preventDefault();
        dispatch(showLoader());

        let url = 'category/update';
        let params = {
            id: id,
            name: name
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/category');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="category">
                <div className="title">Category</div>
                <Card>
                    <CardHeader>Edit</CardHeader>
                    <CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 8, offset: 2 }}>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup className="mt-5">
                                        <Link to="/category" className="btn btn-outline-secondary mr-2">
                                            Cancel
                                        </Link>
                                        <Button type="submit" color="primary" disabled={disable} onClick={(e) => updateCategory(e)} >Submit</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                    </CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default CategoryEdit;
