import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Table, Button } from 'reactstrap';
import { Edit, Trash2 } from 'react-feather';
import { get, post } from '../../../helpers/Request';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { storeToast } from '../../../store/toast/toast';
import { Layout } from '../Layout';
import { Alert } from '../../../components/admin';

function CategoryList() {
    const [categories, setCategories] = useState([]);
    const [openAlert, setOpenAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState(false);
    const [deleteId, setDeleteId] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        getCategories();
    }, []);

    const getCategories = async() => {
        dispatch(showLoader());
        let response = await get('category');
        if (response) {
            if (response.data) {
                setCategories(response.data);
            }
            dispatch(hideLoader());
        }
    }

    const confirmDelete = (item) => {
        setAlertMessage('Are you sure want to delete <b class="text-danger">' + item.name + '</b>?');
        setOpenAlert(true);
        setDeleteId(item.id);
    }

    const closeAlert = () => {
        setDeleteId('');
        setOpenAlert(false);
    }

    const deleteCategory = async () => {
        dispatch(showLoader());

        let url = 'category/delete';
        let params = {
            id: deleteId
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                getCategories();
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
            closeAlert();
        }
    }

    return (
        <Layout>
            <div className="category">
                <div className="title">Category</div>
                <Card>
                	<CardHeader>List</CardHeader>
                	<CardBody>
                		<Table striped>
                            <thead>
                                <tr>
                                    <th className="no">#</th>
                                    <th>Category</th>
                                    <th className="date">Create Date</th>
                                    <th className="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {categories.map((item, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.created_at}</td>
                                    <td>
                                        <Link to={'/category/edit/' + item.id} className="btn btn-primary mr-2">
                                            <Edit color="#fff" size={14} className="mr-2" />
                                            Edit
                                        </Link>
                                        <Button color='danger' onClick={() => confirmDelete(item)} >
                                            <Trash2 color="#fff" size={14} className="mr-2" />
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                	</CardBody>
                </Card>
            </div>

            <Alert
                open={openAlert}
                message={alertMessage}
                onCancel={() => closeAlert()}
                onConfirm={() => deleteCategory()}
            />
        </Layout>
    );
}

export default CategoryList;
