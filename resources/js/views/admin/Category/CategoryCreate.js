import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';

function CategoryCreate() {
    const [name, setName] = useState('');
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (name) {
            setDisable(false);
        }
        else {
            setDisable(true);
        }
    }, [name]);

    const storeCategory = async (e) => {
        e.preventDefault();
        dispatch(showLoader());

        let url = 'category/store';
        let params = {
            name: name
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/category');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="category">
                <div className="title">Category</div>
                <Card>
                	<CardHeader>Create</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup className="mt-5">
                                        <Link to="/category" className="btn btn-outline-secondary mr-2">
                                            Cancel
                                        </Link>
                                        <Button type="submit" color="primary" disabled={disable} onClick={(e) => storeCategory(e)} >Submit</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default CategoryCreate;
