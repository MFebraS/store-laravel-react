import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeUser } from '../../../store/user/user';
import { storeToast } from '../../../store/toast/toast';
import { Toast } from '../../../components/admin';

function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [disable, setDisable] = useState(true);
    const user = useSelector(state => state.user);
    const toast = useSelector(state => state.toast);
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    
    useEffect(() => {
        // show unauthenticated message
        let query = new URLSearchParams(location.search);
        let message = query.get('p');
        if (message) {
            message = message.charAt(0).toUpperCase() + message.slice(1) + '.';
            query.delete('p');
            history.replace({
                search: query.toString(),
            });
            let toast = {
                type: 'error',
                title: 'Error',
                messages: [message]
            }
            dispatch(storeToast(toast));
        }
        if (user) {
            history.push('/dashboard');
        }
        else {
            get('sanctum/csrf-cookie', true);
        }
    }, []);

    useEffect(() => {
        if (email && password) {
            setDisable(false);
        }
        else {
            setDisable(true);
        }
    }, [email, password]);

    const login = async (e) => {
        e.preventDefault();
        if (email && password) {
            let url = 'auth/login';
            let params = {
                email: email,
                password: password
            }
            let response = await post(url, params);
            if (response) {
                if (response.data) {
                    dispatch(storeUser(response.data));
                    let toast = {
                        type: 'success',
                        title: 'Success',
                        messages: ['Login success.']
                    }
                    dispatch(storeToast(toast));
                    history.push('/dashboard');
                }
                else if (response.messages) {
                    let toast = {
                        type: 'error',
                        title: 'Error',
                        messages: response.messages
                    }
                    dispatch(storeToast(toast));
                }
            }
        }
    }

    if (user) {
        return null;
    }

    return (
        <div className="auth">
            <div className="d-flex flex-grow-1 auth-image">
            </div>

            <div className="card">
                <div className="title text-center mb-4">LOGIN</div>
                <Form>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input type="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </FormGroup>
                    <FormGroup>
                        <Label>Password</Label>
                        <Input type="password" name="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </FormGroup>

                    <FormGroup className="mt-5">
                        <Button outline color="secondary" size="lg" block type="submit" disabled={disable} onClick={(e) => login(e)} >Login</Button>
                    </FormGroup>
                </Form>
            </div>

            <Toast type={toast.type} title={toast.title} messages={toast.messages} />
        </div>
    );
}

export default Login;
