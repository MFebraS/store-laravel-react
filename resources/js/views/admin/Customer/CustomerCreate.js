import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap';
import { post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { InputNumeric } from '../../../components/admin';

function CustomerCreate() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');
    const [status, setStatus] = useState(true);
    const [error, setError] = useState('');
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        let _disable = true;
        if (name && email && phone && password && passwordConfirmation) {
            _disable = false;
        }
        setDisable(_disable);

        if (error && password === passwordConfirmation) {
            setError('');
        }
    }, [name, email, phone, password, passwordConfirmation]);

    const storeCustomer = async (e) => {
        e.preventDefault();
        if (password != passwordConfirmation) {
            setError('Password confirmation is not match.');
            return false;
        }

        dispatch(showLoader());

        let url = 'customer/store';
        let params = {
            name: name,
            email: email,
            phone: phone,
            password: password,
            password_confirmation: passwordConfirmation,
            status: status ? 'Active' : 'Suspend'
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/customer');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="customer">
                <div className="title">Customer</div>
                <Card>
                	<CardHeader>Create</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Email *</Label>
                                        <Input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Phone *</Label>
                                        <InputNumeric value={phone} onChange={(e) => setPhone(e)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password</Label>
                                        <Input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password Confirmation</Label>
                                        <Input type="password" value={passwordConfirmation} onChange={(e) => setPasswordConfirmation(e.target.value)} />
                                        {error &&
                                            <div className="text-danger mt-1">{error}</div>
                                        }
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Status (Active/ Suspend) *</Label>
                                        <CustomInput id="status" type="switch" name="status" value="Active" checked={status} onChange={(e) => setStatus(e.target.checked)} />
                                    </FormGroup>
                                    <FormGroup className="mt-5">
                                        <Link to="/Customer" className="btn btn-outline-secondary mr-2">
                                            Cancel
                                        </Link>
                                        <Button type="submit" color="primary" disabled={disable} onClick={(e) => storeCustomer(e)} >Submit</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default CustomerCreate;
