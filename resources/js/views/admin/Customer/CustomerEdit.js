import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { InputNumeric } from '../../../components/admin';

function CustomerEdit() {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPasswordConfirmation, setNewPasswordConfirmation] = useState('');
    const [status, setStatus] = useState(true);
    const [error, setError] = useState('');
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();
    const path_variable = useParams();

    useEffect(() => {
        getCustomer();
    }, []);

    useEffect(() => {
        if (error && newPassword === newPasswordConfirmation) {
            setError('');
        }
    }, [newPassword, newPasswordConfirmation]);

    useEffect(() => {
        let _disable = true;
        if (name && email && phone) {
            _disable = false;
            // required if one of these filled
            if (password || newPassword || newPasswordConfirmation) {
                if (!password || !newPassword || !newPasswordConfirmation) {
                    _disable = true;
                }
            }
        }
        setDisable(_disable);
    }, [name, email, phone, password, newPassword, newPasswordConfirmation]);

    const getCustomer = async () => {
        dispatch(showLoader());

        let response = await get('customer/detail/' + path_variable.id);
        if (response) {
            if (response.status === 'success') {
                let customer = response.data;
                setId(customer.id);
                setName(customer.name);
                setEmail(customer.email || '');
                setPhone(customer.phone || '');
                setStatus(customer.status === 'Active' ? true : false);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const updateCustomer = async (e) => {
        e.preventDefault();
        if (newPassword != newPasswordConfirmation) {
            setError('New password confirmation is not match.');
            return false;
        }

        dispatch(showLoader());

        let url = 'customer/update';
        let params = {
            id: id,
            name: name,
            email: email,
            phone: phone,
            status: status ? 'Active' : 'Suspend'
        }
        if (password) {
            params['password'] = password;
            params['new_password'] = newPassword;
            params['new_password_confirmation'] = newPasswordConfirmation;
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/customer');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="customer">
                <div className="title">Customer</div>
                <Card>
                	<CardHeader>Edit</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Email *</Label>
                                        <Input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Phone *</Label>
                                        <InputNumeric value={phone} onChange={(e) => setPhone(e)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password</Label>
                                        <div className="text-muted mb-1"><i>Fill when change password.</i></div>
                                        <Input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>New Password</Label>
                                        <Input type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>New Password Confirmation</Label>
                                        <Input type="password" value={newPasswordConfirmation} onChange={(e) => setNewPasswordConfirmation(e.target.value)} />
                                        {error &&
                                            <div className="text-danger mt-1">{error}</div>
                                        }
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Status (Active/ Suspend) *</Label>
                                        <CustomInput id="status" type="switch" name="status" value="Active" checked={status} onChange={(e) => setStatus(e.target.checked)} />
                                    </FormGroup>
                                    <FormGroup className="mt-5">
                                        <Link to="/customer" className="btn btn-outline-secondary mr-2">
                                            Cancel
                                        </Link>
                                        <Button type="submit" color="primary" disabled={disable} onClick={(e) => updateCustomer(e)} >Submit</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default CustomerEdit;
