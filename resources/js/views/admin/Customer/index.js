export { default as CustomerList } from './CustomerList';
export { default as CustomerDetail } from './CustomerDetail';
export { default as CustomerCreate } from './CustomerCreate';
export { default as CustomerEdit } from './CustomerEdit';