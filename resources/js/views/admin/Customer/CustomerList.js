import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Table, Badge, Button, Form, Input } from 'reactstrap';
import { Eye, Edit, Trash2 } from 'react-feather';
import { get, post } from '../../../helpers/Request';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { storeToast } from '../../../store/toast/toast';
import { Layout } from '../Layout';
import { Alert, Pagination } from '../../../components/admin';

function CustomerList() {
    const [customers, setCustomers] = useState([]);
    const [keyword, setKeyword] = useState('');
    const [links, setLinks] = useState({});
    const [openAlert, setOpenAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState(false);
    const [deleteId, setDeleteId] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        getCustomers();
    }, []);

    useEffect(() => {
        getCustomers();
    }, [keyword]);

    const getCustomers = async (page='') => {
        dispatch(showLoader());

        let url = 'customer?';
        if (keyword) {
            url += 'search=' + keyword;
        }
        if (page) {
            if (keyword) {
                url += '&';
            }
            url += 'page=' + page;
        }

        let response = await get(url);
        if (response) {
            if (response.data) {
                setCustomers(response.data);
                setLinks(response.links);
            }
            dispatch(hideLoader());
        }
    }

    const confirmDelete = (item) => {
        setAlertMessage('Are you sure want to delete <b class="text-danger">' + item.name + '</b>?');
        setOpenAlert(true);
        setDeleteId(item.id);
    }

    const closeAlert = () => {
        setDeleteId('');
        setOpenAlert(false);
    }

    const deleteCustomer = async () => {
        dispatch(showLoader());

        let url = 'customer/delete';
        let params = {
            id: deleteId
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                getCustomers();
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
            closeAlert();
        }
    }

    return (
        <Layout>
            <div className="customer">
                <div className="title">Customer</div>
                <Card>
                	<CardHeader>List</CardHeader>
                	<CardBody>
                        <Form className="mb-3" onSubmit={(e) => e.preventDefault() }>
                            <Input className="search" type="text" placeholder="Search..." onChange={(e) => setKeyword(e.target.value)} />
                        </Form>

                		<Table striped>
                            <thead>
                                <tr>
                                    <th className="no">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th className="date">Register Date</th>
                                    <th style={{ width: '300px' }}>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {customers.map((item, index) => (
                                <tr key={index}>
                                    <td>{links.last_entry >= 0 && links.last_entry + index + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>{item.phone}</td>
                                    <td>
                                        <Badge color={item.status === 'Active' ? 'primary' : 'warning'} pill>{item.status}</Badge>
                                    </td>
                                    <td>{item.created_at}</td>
                                    <td>
                                        <Link to={'/customer/detail/' + item.id} className="btn btn-outline-primary mr-2">
                                            <Eye color="#31a2ff" size={14} className="mr-2" />
                                            Detail
                                        </Link>
                                        <Link to={'/customer/edit/' + item.id} className="btn btn-primary mr-2">
                                            <Edit color="#fff" size={14} className="mr-2" />
                                            Edit
                                        </Link>
                                        <Button color='danger' onClick={() => confirmDelete(item)} >
                                            <Trash2 color="#fff" size={14} className="mr-2" />
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>

                        <Pagination
                            first={links.first_item}
                            last={links.last_item}
                            total={links.total}
                            disablePrevButton={links.prev_page_url ? false : true}
                            disableNextButton={links.next_page_url ? false : true}
                            onPrevClick={() => getCustomers(links.current_page - 1)}
                            onNextClick={() => getCustomers(links.current_page + 1)}
                        />
                	</CardBody>
                </Card>
            </div>

            <Alert
                open={openAlert}
                message={alertMessage}
                onCancel={() => closeAlert()}
                onConfirm={() => deleteCustomer()}
            />
        </Layout>
    );
}

export default CustomerList;
