import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Badge, Button } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { Alert } from '../../../components/admin';

function CustomerDetail() {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [status, setStatus] = useState('');
    const [addresses, setAddresses] = useState([]);
    const [deleteId, setDeleteId] = useState('');
    const [openAlert, setOpenAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();
    const path_variable = useParams();

    useEffect(() => {
        getCustomer();
        getAddresses();
    }, []);

    const getCustomer = async () => {
        dispatch(showLoader());

        let response = await get('customer/detail/' + path_variable.id);
        if (response) {
            if (response.status === 'success') {
                let customer = response.data;
                setId(customer.id);
                setName(customer.name);
                setEmail(customer.email);
                setStatus(customer.status);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const getAddresses = async () => {
        dispatch(showLoader());

        let response = await get('address/' + path_variable.id);
        if (response) {
            if (response.status === 'success') {
                setAddresses(response.data);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const addAddress = () => {
        let location = {
            pathname: '/address/create',
            state: {
                user_id: id,
                user_name: name
            }
        }
        history.push(location);
    }

    const editAddress = (address_id) => {
        let location = {
            pathname: '/address/edit/' + address_id,
            state: {
                user_id: id,
            }
        }
        history.push(location);
    }

    const confirmDelete = (item) => {
        setAlertMessage('Are you sure want to delete <b class="text-danger">' + item.label + '</b>?');
        setOpenAlert(true);
        setDeleteId(item.id);
    }

    const closeAlert = () => {
        setDeleteId('');
        setOpenAlert(false);
    }

    const deleteAddress = async () => {
        dispatch(showLoader());

        let url = 'address/delete';
        let params = {
            id: deleteId
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                getAddresses();
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
            closeAlert();
        }
    }

    return (
        <Layout>
            <div className="customer detail">
                <div className="title">Customer</div>
                <Card>
                	<CardHeader className="d-flex justify-content-between align-items-center">
                        <div>Detail</div>
                        <div>
                            <Link to={'/customer/edit/' + id} className="btn btn-sm btn-outline-primary mr-2">
                                Edit
                            </Link>
                            <Button outline color="primary" size="sm" onClick={() => addAddress()}>
                                Add Address
                            </Button>
                        </div>
                    </CardHeader>
                	<CardBody>
                        <div className="d-flex flex-row">
                            <div className="attribute">Name</div>
                            <div className="semicolon">:</div>
                            <div>{name}</div>
                        </div>
                        <div className="d-flex flex-row">
                            <div className="attribute">Email</div>
                            <div className="semicolon">:</div>
                            <div>{email}</div>
                        </div>
                        <div className="d-flex flex-row">
                            <div className="attribute">Status</div>
                            <div className="semicolon">:</div>
                            <div>
                                <Badge color={status === 'Active' ? 'primary' : 'warning'} pill>{status}</Badge>
                            </div>
                        </div>

                        <div className="address mt-4">
                            {addresses.map((item, index) => (
                                <div className="address-item d-flex flex-row justify-content-between mb-3" key={index}>
                                    <div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">Label</div>
                                            <div className="semicolon">:</div>
                                            <div>{item.label}</div>
                                        </div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">Address</div>
                                            <div className="semicolon">:</div>
                                            <div>{item.address}</div>
                                        </div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">Province</div>
                                            <div className="semicolon">:</div>
                                            <div>{item.province}</div>
                                        </div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">City</div>
                                            <div className="semicolon">:</div>
                                            <div>{item.city}</div>
                                        </div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">Note</div>
                                            <div className="semicolon">:</div>
                                            <div>{item.note || '-'}</div>
                                        </div>
                                        <div className="d-flex flex-row">
                                            <div className="attribute">Default</div>
                                            <div className="semicolon">:</div>
                                            <div>
                                                <Badge color={item.default ? 'success' : 'secondary'} pill>{item.default ? 'Yes' : 'No'}</Badge>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <Button className="mr-2" color="primary" size="sm" onClick={() => editAddress(item.id)}>
                                            Edit
                                        </Button>
                                        <Button color="danger" size="sm" onClick={() => confirmDelete(item)}>
                                            Delete
                                        </Button>
                                    </div>
                                </div>
                            ))}
                        </div>
                	</CardBody>
                </Card>
            </div>

            <Alert
                open={openAlert}
                message={alertMessage}
                onCancel={() => closeAlert()}
                onConfirm={() => deleteAddress()}
            />
        </Layout>
    );
}

export default CustomerDetail;
