import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { InputImageRepeater, InputNumber, Select } from '../../../components/admin';

function ProductCreate() {
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState('');
    const [name, setName] = useState('');
    const [code, setCode] = useState('');
    const [price, setPrice] = useState('');
    const [stock, setStock] = useState('');
    const [weight, setWeight] = useState('');
    const [images, setImages] = useState([{ image: null }]);
    const [description, setDescription] = useState('');
    const [status, setStatus] = useState(true);
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        loadOptions();
    }, []);

    useEffect(() => {
        let _disable = true;
        if (category && name && price && weight && stock) {
            _disable = validateImages();
        }
        setDisable(_disable);
    }, [category, name, price, stock, weight, status, images]);

    const loadOptions = async () => {
        dispatch(showLoader());

        let response = await get('product/category');
        if (response) {
            if (response.status === 'success') {
                setCategories(response.data);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const validateImages = () => {
        let _disable = false;
        images.every(function(item, index) {
            if (!item.image) {
                _disable = true;

                // stop loop
                return false;
            }
        });
        return _disable;
    }

    const changeImages = (image, index) => {
        let temp = [...images];
        temp[index]['image'] = image;
        setImages(temp);
    }

    const addImage = () => {
        setImages(prevState => [...prevState, { image: null }]);
    }

    const removeImage = (index) => {
        if (images.length > 1 && confirm('Are you sure want to delete this element?')) {
            let temp = [...images];
            temp.splice(index, 1);
            setImages(temp);
        }
    }

    const storeProduct = async (e) => {
        e.preventDefault();
        dispatch(showLoader());

        let _images = [];
        images.forEach(function(item, index) {
            if (item.image) {
                _images.push(item.image);
            }
        });

        let url = 'product/store';
        let params = {
            category_id: category.value,
            name: name,
            code: code,
            price: price,
            stock: stock,
            weight: weight,
            images: _images,
            description: description,
            status: status ? 'Show' : 'Hidden',
        }
        let response = await post(url, params, false, 'form-data');
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/product');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="product">
                <div className="title">Product</div>
                <Card>
                	<CardHeader>Create</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Category *</Label>
                                        <Select
                                            options={categories}
                                            onChange={(e) => setCategory(e)}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Code</Label>
                                        <Input type="text" name="code" value={code} onChange={(e) => setCode(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Price *</Label>
                                        <InputNumber name="price" value={price} onChange={(e) => setPrice(e)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Description</Label>
                                        <Input type="textarea" rows="3" name="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                                    </FormGroup>
                                </Col>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Weight (Gram) *</Label>
                                        <InputNumber name="weight" value={weight} onChange={(e) => setWeight(e)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Stock *</Label>
                                        <InputNumber name="stock" value={stock} onChange={(e) => setStock(e)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Status (Show/ Hide) *</Label>
                                        <CustomInput id="status" type="switch" name="status" value="Show" checked={status} onChange={(e) => setStatus(e.target.checked)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Images *</Label>
                                        <div className="image-repeater d-flex flex-row flex-wrap">
                                            {images.map((item, index) => (
                                                <InputImageRepeater
                                                    key={index}
                                                    image={item.image}
                                                    onChange={(e) => changeImages(e, index)}
                                                    onDelete={() => removeImage(index)}
                                                />
                                            ))}
                                        </div>
                                        <Button color="success" size="sm" onClick={(e) => addImage(e)} >Add</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup className="mt-5">
                                <Link to="/product" className="btn btn-outline-secondary mr-2">
                                    Cancel
                                </Link>
                                <Button type="submit" color="primary" disabled={disable} onClick={(e) => storeProduct(e)} >Submit</Button>
                            </FormGroup>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default ProductCreate;
