import React from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import { Layout } from '../Layout';

function Dashboard() {
    return (
        <Layout>
            <div className="dashboard">
                <div className="title">Dashboard</div>
                <Card>
                	<CardHeader>Title</CardHeader>
                	<CardBody>
                		Lorem
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default Dashboard;
