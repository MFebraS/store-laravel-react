import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap';
import { MapContainer, TileLayer } from 'react-leaflet';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { Select, DraggableMarker } from '../../../components/admin';
import 'leaflet/dist/leaflet.css';

function AddressCreate() {
    const [userId, setUserId] = useState('');
    const [userName, setUserName] = useState('');
    const [provinces, setProvinces] = useState([]);
    const [cities, setCities] = useState('');
    const [options, setOptions] = useState([]);
    const [label, setLabel] = useState('');
    const [cityId, setCityId] = useState([]);
    const [address, setAddress] = useState('');
    const [latitude, setLatitude] = useState(-7.811478724435832);
    const [longitude, setLongitude] = useState(110.37705371485775);
    const [note, setNote] = useState('');
    const [isDefault, setIsDefault] = useState(false);
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const location = useLocation();
    const history = useHistory();

    if (!location) {
        return null;
    }

    useEffect(() => {
        if (!location.state) {
            history.goBack();
        }
        else {
            setUserId(location.state.user_id);
            setUserName(location.state.user_name);

            getCities();
        }
    }, []);

    useEffect(() => {
        if (userId && label && cityId && address && latitude && longitude) {
            setDisable(false);
        }
        else {
            setDisable(true);
        }
    }, [userId, label, cityId, address, latitude, longitude]);

    const getCities = async () => {
        dispatch(showLoader());

        let response = await get('address/cities');
        if (response) {
            if (response.status === 'success') {
                setProvinces(response.data.provinces);
                setCities(response.data.cities);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const changeProvince = (e) => {
        setCityId('');
        setOptions(cities[e.value]);
    }

    const changeMap = (coordinate) => {
        setLatitude(coordinate.lat);
        setLongitude(coordinate.lng);
    }

    const storeAddress = async (e) => {
        e.preventDefault();
        dispatch(showLoader());

        let url = 'address/store';
        let params = {
            user_id: userId,
            label: label,
            city_id: cityId,
            address: address,
            latitude: latitude,
            longitude: longitude,
            note: note,
            default: isDefault,
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/customer/detail/' + userId);
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const center = {
        lat: latitude,
        lng: longitude,
    }

    return (
        <Layout>
            <div className="address">
                <div className="title">Address</div>
                <Card>
                	<CardHeader>Create</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Customer *</Label>
                                        <Input type="text" value={userName} readOnly={true} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Label *</Label>
                                        <Input type="text" value={label} onChange={(e) => setLabel(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Province *</Label>
                                        <Select
                                            options={provinces}
                                            onChange={(e) => changeProvince(e)}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>City *</Label>
                                        <Select
                                            options={options}
                                            onChange={(e) => setCityId(e.value)}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Address *</Label>
                                        <Input type="text" value={address} onChange={(e) => setAddress(e.target.value)} />
                                    </FormGroup>
                                </Col>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Map *</Label>
                                        <MapContainer center={center} zoom={13} scrollWheelZoom={false} style={{ height:'290px' }}>
                                            <TileLayer
                                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                            />
                                            <DraggableMarker center={center} onDragend={(e) => changeMap(e) } />
                                        </MapContainer>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Note</Label>
                                        <Input type="text" value={note} onChange={(e) => setNote(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Default</Label>
                                        <CustomInput id="default" type="switch" value="Default" checked={isDefault} onChange={(e) => setIsDefault(e.target.checked)} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup className="mt-5">
                                <Link to={'/customer/detail/' + userId} className="btn btn-outline-secondary mr-2">
                                    Cancel
                                </Link>
                                <Button type="submit" color="primary" disabled={disable} onClick={(e) => storeAddress(e)} >Submit</Button>
                            </FormGroup>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default AddressCreate;
