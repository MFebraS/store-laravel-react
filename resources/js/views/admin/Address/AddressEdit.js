import React, { useState, useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap';
import { MapContainer, TileLayer } from 'react-leaflet';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';
import { Select, DraggableMarker } from '../../../components/admin';
import 'leaflet/dist/leaflet.css';

function AddressEdit() {
    const [userId, setUserId] = useState('');
    const [userName, setUserName] = useState('');
    const [provinces, setProvinces] = useState([]);
    const [province, setProvince] = useState('');
    const [cities, setCities] = useState('');
    const [city, setCity] = useState('');
    const [options, setOptions] = useState([]);
    const [id, setId] = useState('');
    const [label, setLabel] = useState('');
    const [address, setAddress] = useState('');
    const [latitude, setLatitude] = useState(-7.811478724435832);
    const [longitude, setLongitude] = useState(110.37705371485775);
    const [note, setNote] = useState('');
    const [isDefault, setIsDefault] = useState(false);
    const [disable, setDisable] = useState(true);
    const [recenter, setRecenter] = useState('a');
    const dispatch = useDispatch();
    const location = useLocation();
    const history = useHistory();
    const path_variable = useParams();

    if (!location) {
        return null;
    }

    useEffect(() => {
        if (!location.state) {
            history.goBack();
        }
        else {
            getCities();
            getAddress();
        }
    }, []);

    useEffect(() => {
        if (typeof province === 'object' && typeof cities === 'object') {
            setOptions(cities[province.value]);
        }
    }, [province, cities]);

    useEffect(() => {
        if (id && label && city && address && latitude && longitude) {
            setDisable(false);
        }
        else {
            setDisable(true);
        }
    }, [id, label, city, address, latitude, longitude]);

    const getCities = async () => {
        dispatch(showLoader());

        let response = await get('address/cities');
        if (response) {
            if (response.status === 'success') {
                setProvinces(response.data.provinces);
                setCities(response.data.cities);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const getAddress = async () => {
        dispatch(showLoader());

        let response = await get('address/edit/' + path_variable.id);
        if (response) {
            if (response.status === 'success') {
                let address = response.data;
                setUserId(address.user.id);
                setUserName(address.user.name);
                setId(address.id);
                setLabel(address.label);
                setProvince({ value: address.city.province_id, label: address.city.province });
                setCity({ value: address.city_id, label: address.city.type +' '+ address.city.city_name })
                setAddress(address.address);
                setLatitude(parseFloat(address.latitude));
                setLongitude(parseFloat(address.longitude));
                setNote(address.note || '');
                setIsDefault(address.default);
                setRecenter('b');
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const changeProvince = (e) => {
        setCity('');
        setProvince(e)
        setOptions(cities[e.value]);
    }

    const changeMap = (coordinate) => {
        setLatitude(coordinate.lat);
        setLongitude(coordinate.lng);
    }

    const updateAddress = async (e) => {
        e.preventDefault();
        dispatch(showLoader());

        let url = 'address/update';
        let params = {
            id: id,
            label: label,
            city_id: city.value,
            address: address,
            latitude: latitude,
            longitude: longitude,
            note: note,
            default: isDefault,
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/customer/detail/' + location.state.user_id);
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const center = {
        lat: latitude,
        lng: longitude,
    }

    return (
        <Layout>
            <div className="address">
                <div className="title">Address</div>
                <Card>
                	<CardHeader>Edit</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Customer *</Label>
                                        <Input type="text" value={userName} readOnly={true} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Label *</Label>
                                        <Input type="text" value={label} onChange={(e) => setLabel(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Province *</Label>
                                        <Select
                                            value={province}
                                            options={provinces}
                                            onChange={(e) => changeProvince(e)}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>City *</Label>
                                        <Select
                                            value={city}
                                            options={options}
                                            onChange={(e) => setCity(e)}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Address *</Label>
                                        <Input type="text" value={address} onChange={(e) => setAddress(e.target.value)} />
                                    </FormGroup>
                                </Col>
                                <Col md={{ size: 6 }}>
                                    <FormGroup>
                                        <Label>Map *</Label>
                                        <MapContainer key={recenter} center={center} zoom={13} scrollWheelZoom={false} style={{ height:'290px' }}>
                                            <TileLayer
                                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                            />
                                            <DraggableMarker center={center} onDragend={(e) => changeMap(e) } />
                                        </MapContainer>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Note</Label>
                                        <Input type="text" value={note} onChange={(e) => setNote(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Default</Label>
                                        <CustomInput id="default" type="switch" value="Default" checked={isDefault} onChange={(e) => setIsDefault(e.target.checked)} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup className="mt-5">
                                <Link to={'/customer/detail/' + userId} className="btn btn-outline-secondary mr-2">
                                    Cancel
                                </Link>
                                <Button type="submit" color="primary" disabled={disable} onClick={(e) => updateAddress(e)} >Submit</Button>
                            </FormGroup>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default AddressEdit;
