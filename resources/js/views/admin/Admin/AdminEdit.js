import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { get, post } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { showLoader, hideLoader } from '../../../store/loader/loader';
import { Layout } from '../Layout';

function AdminEdit() {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPasswordConfirmation, setNewPasswordConfirmation] = useState('');
    const [error, setError] = useState('');
    const [disable, setDisable] = useState(true);
    const dispatch = useDispatch();
    const history = useHistory();
    const path_variable = useParams();

    useEffect(() => {
        getAdmin();
    }, []);

    useEffect(() => {
        let _disable = true;
        if (name && email) {
            _disable = false;
            // required if one of these filled
            if (password || newPassword || newPasswordConfirmation) {
                if (!password || !newPassword || !newPasswordConfirmation) {
                    _disable = true;
                }
            }
        }
        setDisable(_disable);

        if (error && newPassword === newPasswordConfirmation) {
            setError('');
        }
    }, [name, email, password, newPassword, newPasswordConfirmation]);

    const getAdmin = async () => {
        dispatch(showLoader());

        let response = await get('admin/edit/' + path_variable.id);
        if (response) {
            if (response.status === 'success') {
                let admin = response.data;
                setId(admin.id);
                setName(admin.name);
                setEmail(admin.email);
            }
            else {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    const updateAdmin = async (e) => {
        e.preventDefault();
        if (newPassword != newPasswordConfirmation) {
            setError('New password confirmation is not match.');
            return false;
        }

        dispatch(showLoader());

        let url = 'admin/update';
        let params = {
            id: id,
            name: name,
            email: email
        }
        if (password) {
            params['password'] = password;
            params['new_password'] = newPassword;
            params['new_password_confirmation'] = newPasswordConfirmation;
        }
        let response = await post(url, params);
        if (response) {
            let toast = {
                messages: response.messages
            }
            if (response.status === 'success') {
                toast.type = 'success';
                toast.title = 'Success';
                dispatch(storeToast(toast));
                history.push('/admin');
            }
            else {
                toast.type = 'error';
                toast.title = 'Error';
                dispatch(storeToast(toast));
            }
            dispatch(hideLoader());
        }
    }

    return (
        <Layout>
            <div className="admin">
                <div className="title">Admin</div>
                <Card>
                	<CardHeader>Edit</CardHeader>
                	<CardBody>
                        <Form>
                            <Row>
                                <Col md={{ size: 6, offset: 3 }}>
                                    <FormGroup>
                                        <Label>Name *</Label>
                                        <Input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Email *</Label>
                                        <Input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Password</Label>
                                        <div className="text-muted mb-1"><i>Fill when change password.</i></div>
                                        <Input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>New Password</Label>
                                        <Input type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>New Password Confirmation</Label>
                                        <Input type="password" value={newPasswordConfirmation} onChange={(e) => setNewPasswordConfirmation(e.target.value)} />
                                        {error &&
                                            <div className="text-danger mt-1">{error}</div>
                                        }
                                    </FormGroup>
                                    <FormGroup className="mt-5">
                                        <Link to="/admin" className="btn btn-outline-secondary mr-2">
                                            Cancel
                                        </Link>
                                        <Button type="submit" color="primary" disabled={disable} onClick={(e) => updateAdmin(e)} >Submit</Button>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>
                	</CardBody>
                </Card>
            </div>
        </Layout>
    );
}

export default AdminEdit;
