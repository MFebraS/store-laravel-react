import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { Monitor, User, Users, Tag, Box, ShoppingCart, MessageSquare, Settings, LogOut } from 'react-feather';
import { get } from '../../../helpers/Request';
import { storeToast } from '../../../store/toast/toast';
import { clearUser } from '../../../store/user/user';
import { Toast, Collapse, Loader } from '../../../components/admin';

function Layout({ children }) {
    const user = useSelector(state => state.user);
    const toast = useSelector(state => state.toast);
    const dispatch = useDispatch();
    const history = useHistory();

    const logout = async () => {
        let response = await get('auth/logout');
        if (response) {
            if (response.status === 'success') {
                dispatch(clearUser());
                let toast = {
                    type: 'success',
                    title: 'Success',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
                history.push('/');
            }
            else if (response.messages) {
                let toast = {
                    type: 'error',
                    title: 'Error',
                    messages: response.messages
                }
                dispatch(storeToast(toast));
            }
        }
    }

    const isActiveMenu = (menu) => {
        let route = history.location.pathname;
        return route.substr(-menu.length) === menu ? 'active' : '';
    }

    const isActiveCollapse = (menu) => {
        let route = history.location.pathname;
        return route.includes(menu) ? 'active' : '';
    }

    return (
        <div>
            <aside className="sidebar">
                <div className="brand">
                    <h3 className="text-center mb-0">STORE</h3>
                </div>
                <ul className="nav flex-column">
                    <li className={'nav-item ' + isActiveMenu('dashboard')}>
                        <Link className="nav-link d-flex align-items-center" to="/dashboard">
                            <Monitor color="#8c929d" size={18} className="mr-3" />
                            Dashboard
                        </Link>
                    </li>
                    <li className={'nav-item ' + isActiveCollapse('admin')}>
                        <Collapse
                            open={isActiveCollapse('admin')}
                            titleClass="nav-link d-flex align-items-center"
                            bodyClass="submenu"
                            renderTitle={(title) => (
                                <Fragment>
                                    <User color="#8c929d" size={18} className="mr-3" />
                                    Admin
                                </Fragment>
                            )}
                            renderBody={(body) => (
                                <Fragment>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('admin')} to="/admin">
                                        List
                                    </Link>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('admin/create')} to="/admin/create">
                                        Create
                                    </Link>
                                </Fragment>
                            )}
                        />
                    </li>
                    <li className={'nav-item ' + isActiveCollapse('customer')}>
                        <Collapse
                            open={isActiveCollapse('customer')}
                            titleClass="nav-link d-flex align-items-center"
                            bodyClass="submenu"
                            renderTitle={(title) => (
                                <Fragment>
                                    <Users color="#8c929d" size={18} className="mr-3" />
                                    Customer
                                </Fragment>
                            )}
                            renderBody={(body) => (
                                <Fragment>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('customer')} to="/customer">
                                        List
                                    </Link>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('customer/create')} to="/customer/create">
                                        Create
                                    </Link>
                                </Fragment>
                            )}
                        />
                    </li>
                    <li className={'nav-item ' + isActiveCollapse('category')}>
                        <Collapse
                            open={isActiveCollapse('category')}
                            titleClass="nav-link d-flex align-items-center"
                            bodyClass="submenu"
                            renderTitle={(title) => (
                                <Fragment>
                                    <Tag color="#8c929d" size={18} className="mr-3" />
                                    Category
                                </Fragment>
                            )}
                            renderBody={(body) => (
                                <Fragment>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('category')} to="/category">
                                        List
                                    </Link>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('category/create')} to="/category/create">
                                        Create
                                    </Link>
                                </Fragment>
                            )}
                        />
                    </li>
                    <li className={'nav-item ' + isActiveCollapse('product')}>
                        <Collapse
                            open={isActiveCollapse('product')}
                            titleClass="nav-link d-flex align-items-center"
                            bodyClass="submenu"
                            renderTitle={(title) => (
                                <Fragment>
                                    <Box color="#8c929d" size={18} className="mr-3" />
                                    Product
                                </Fragment>
                            )}
                            renderBody={(body) => (
                                <Fragment>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('product')} to="/product">
                                        List
                                    </Link>
                                    <Link className={'nav-link d-flex align-items-center ' + isActiveMenu('product/create')} to="/product/create">
                                        Create
                                    </Link>
                                </Fragment>
                            )}
                        />
                    </li>
                    <li className={'nav-item ' + isActiveMenu('order')}>
                        <Link className="nav-link d-flex align-items-center" to="/order">
                            <ShoppingCart color="#8c929d" size={18} className="mr-3" />
                            Order
                        </Link>
                    </li>
                    <li className={'nav-item ' + isActiveMenu('chat')}>
                        <Link className="nav-link d-flex align-items-center" to="/chat">
                            <MessageSquare color="#8c929d" size={18} className="mr-3" />
                            Chat
                        </Link>
                    </li>
                    <li className={'nav-item ' + isActiveMenu('settings')}>
                        <Link className="nav-link d-flex align-items-center" to="/settings">
                            <Settings color="#8c929d" size={18} className="mr-3" />
                            Settings
                        </Link>
                    </li>
                </ul>
            </aside>

            <div>
                <header className="header d-flex align-items-center justify-content-end">
                    <div className="header-item">Hi, {user.name}</div>
                    <button className="ml-3 btn btn-icon" onClick={logout} >
                        <LogOut color="#ffffff" size={18} />
                    </button>
                </header>

                <div className="main">
                    {children}

                    <Loader/>
                </div>

                <footer className="footer d-flex align-items-center">
                    &copy; 2020 Store
                </footer>
            </div>

            <Toast type={toast.type} title={toast.title} messages={toast.messages} />
        </div>
    );
}

export default Layout;
