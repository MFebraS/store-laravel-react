import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import { Provider } from 'react-redux';
import store from './store';

function App() {
    return (
        <React.StrictMode>
            <Router/>
        </React.StrictMode>
    );
}

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(
    	<Provider store={store}>
    		<App />
    	</Provider>,
    	document.getElementById('root')
    );
}
