import React, { useState, useEffect, useRef } from 'react';
import { ChevronDown } from 'react-feather';

function Select({ className='', value, options, onChange=null }) {
    const [active, setActive] = useState(0);
    const [dropdown, setDropdown] = useState(false);
    const [selectedValue, setSelectedValue] = useState(value);
    const [optionList, setOptionList] = useState(options);
    const [keyword, setKeyword] = useState('');
    const inputRef = useRef();
    const interfaceRef = useRef();

    useEffect(() => {
        document.addEventListener('click', handleClick);
        return () => {
            document.removeEventListener('click', handleClick);
        };
    }, []);

    useEffect(() => {
        setOptionList(options);
    }, [options]);
    
    useEffect(() => {
        setSelectedValue(value);
        let index = options.findIndex(option => option.value === value.value);
        setActive(index);
    }, [value]);

    const handleClick = (e) => {
        // close dropdown when outside click
        if (interfaceRef.current && !interfaceRef.current.contains(e.target)) {
            setDropdown(false);
        }
    }

    const focusInput = () => {
        inputRef.current.focus();
    }

    const handleKeyDown = (e) => {
        if (e.keyCode === 38) {
            // arrow up
            let maxIndex = optionList.length - 1;
            setActive(prevActive => prevActive === 0 ? maxIndex : prevActive - 1);
        }
        else if (e.keyCode === 40) {
            // arrow down
            let maxIndex = optionList.length - 1;
            setActive(prevActive => prevActive === maxIndex ? 0 : prevActive + 1);
        }
        else if (e.keyCode === 13) {
            // enter
            select(active);
        }
    }

    const search = (val) => {
        if (val.length === 0) {
            setOptionList(options);
        }
        else {
            let regex = new RegExp(val, "i");
            let result = options.filter((option) => {
                return option.label.match(regex);
            });
            setOptionList(result);
        }
        setKeyword(val);
    }

    const select = (index) => {
        let selected = optionList[index];
        setSelectedValue(selected);
        setDropdown(false);
        setKeyword('');
        setOptionList(options);
        index = options.findIndex(option => option.value === selected.value);
        setActive(index);
        onChange(selected);
    }

	return (
        <div className={'c-select form-control ' + className}>
            <div className="c-select-input-interface" ref={interfaceRef} onClick={() => focusInput()}></div>
            {selectedValue && keyword.length === 0 &&
                <div className="c-select-input-selected">{selectedValue.label}</div>
            }
            <div className="c-select-input-wrapper d-flex flex-row">
        		<input
                    className="c-select-input flex-grow-1"
                    type="text"
                    value={keyword}
                    ref={inputRef}
                    onChange={(e) => search(e.target.value)}
                    onFocus={() => setDropdown(true)}
                    onKeyDown={(e) => handleKeyDown(e)}
                />
                <div>
                    <ChevronDown color="#8c929d" size={18} />
                </div>
            </div>
            <div className={'c-select-dropdown mt-3 ' + (dropdown ? 'open' : '')}>
                <ul>
                    {optionList.length > 0 ?
                        optionList.map((item, index) => (
                            <li
                                key={index}
                                className={active === index ? 'active' : ''}
                                onClick={() => select(index)} >
                                {item.label}
                            </li>
                        ))
                        :
                        <li>No Options</li>
                    }
                </ul>
            </div>
        </div>
	);
}

export default Select;