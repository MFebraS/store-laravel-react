import React, { useState, useEffect } from 'react';
import { Trash2, Upload } from 'react-feather';

function InputImageRepeater({ image:img, mode='input', onChange=null, onDelete=null }) {
    const [image, setImage] = useState('');

    useEffect(() => {
        if (img && typeof img === 'object') {
            let reader = new FileReader();

            reader.onloadend = () => {
                setImage(reader.result);
            }

            reader.readAsDataURL(img); // convert to base64 string
        }
        else {
            setImage(img);
        }
    }, [img]);

	return (
		<div className="item d-flex justify-content-center align-items-center">
            {image &&
                <img className="preview" src={image} alt="" />
            }
            {mode === 'input' &&
                <input className="input" type="file" onChange={(e) => onChange(e.target.files[0])} />
            }
            <Upload color="#8c929d" size={30} />
            <div className="btn btn-danger btn-delete" onClick={() => onDelete()}>
                <Trash2 color="#ffffff" size={14} />
            </div>
        </div>
	);
}

export default InputImageRepeater;