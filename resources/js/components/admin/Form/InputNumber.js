import React, { Fragment, useState, useEffect } from 'react';
import { numberFormat } from '../../../helpers/Helper';

function InputNumber({ className='', name='', value='', onChange=null }) {
    const [numberStr, setNumberStr] = useState(numberFormat(value));
    const [error, setError] = useState('');
    // check if number
    const regex = /[\d.]+(?:e-?\d+)?$/;

    useEffect(() => {
        // value changed from props
        let val = value.toString();
        if (regex.test(val)) {
            val = val.replace(/\./g, '');
            let val_str = numberFormat(val);
            setNumberStr(val_str);
        }
        else if (val === '') {
            setNumberStr(val);
        }
    }, [value]);

    const handleChange = (val) => {
        if (regex.test(val)) {
            val = val.replace(/\./g, '');
            let val_str = numberFormat(val);
            setNumberStr(val_str);
            onChange(val);
            setError('');
        }
        else if (val === '') {
            setNumberStr(val);
            onChange(val);
            setError('');
        }
        else {
            setError('Please insert number for this field.');
        }
    }

	return (
        <Fragment>
    		<input
                className={'form-control ' + className}
                type="text"
                inputMode="numeric"
                name={name}
                value={numberStr}
                onChange={(e) => handleChange(e.target.value)}
            />
            {error &&
                <div className="text-danger mt-1">{error}</div>
            }
        </Fragment>
	);
}

export default InputNumber;