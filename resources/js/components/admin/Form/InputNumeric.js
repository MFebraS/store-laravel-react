import React, { Fragment, useState, useEffect } from 'react';

function InputNumeric({ className='', name='', value='', onChange=null }) {
    const [number, setNumber] = useState(value);
    const [error, setError] = useState('');
    // check if number
    const regex = /[\d.]+(?:e-?\d+)?$/;

    useEffect(() => {
        // value changed from props
        let val = value.toString();
        if (regex.test(val)) {
            setNumber(val);
        }
        else if (val === '') {
            setNumber(val);
        }
    }, [value]);

    const handleChange = (val) => {
        if (regex.test(val)) {
            setNumber(val);
            onChange(val);
            setError('');
        }
        else if (val === '') {
            setNumber(val);
            onChange(val);
            setError('');
        }
        else {
            setError('Please insert number for this field.');
        }
    }

	return (
        <Fragment>
    		<input
                className={'form-control ' + className}
                type="text"
                inputMode="numeric"
                name={name}
                value={number}
                onChange={(e) => handleChange(e.target.value)}
            />
            {error &&
                <div className="text-danger mt-1">{error}</div>
            }
        </Fragment>
	);
}

export default InputNumeric;