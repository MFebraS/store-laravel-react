import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

function Loader() {
    const loader = useSelector(state => state.loader);

	if (loader <= 0) {
		return null;
	}

	return (
		<div className="loader d-flex justify-content-center align-items-center">
            <div className="loadingio-spinner-dual-ring-yvvns0eno3f">
                <div className="ldio-udqwbl5sat8">
                    <div></div><div><div></div></div>
                </div>
            </div>
        </div>
	);
}

export default Loader;