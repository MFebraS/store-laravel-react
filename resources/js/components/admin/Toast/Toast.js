import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { clearToast } from '../../../store/toast/toast';

function Toast({ type, title, messages }) {
    const [hiddenClass, setHiddenClass] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        if (messages) {
        	// show toast
			setHiddenClass('');
            
            // auto hide toast
            let timeout = setTimeout(() => {
    			closeToast();
    		}, 2000);
            
            return () => {
                clearTimeout(timeout)
            }
        }
    }, [messages]);

	const closeToast = () => {
		setHiddenClass('hidden');
		setTimeout(() => {
			dispatch(clearToast());
		}, 200);
	}

	if (!messages) {
		return null;
	}

	return (
		<div className={"toast " + hiddenClass}>
            <div className="toast-header d-flex justify-content-between align-items-center">
            	<div>
            		<span className={"mr-2 toast-bullet " + type}></span>
            		<strong>{title}</strong>
            	</div>
            	<button type="button" className="close" aria-label="Close" onClick={() => closeToast()}>
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            
            <div className="toast-body">
                {messages.map((item, index) => (
                    <div key={index}>{item}</div>
                ))}
            </div>
        </div>
	);
}

export default Toast;