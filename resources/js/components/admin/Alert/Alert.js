import React, { useState, useEffect } from 'react';
import { Card, CardHeader, CardBody, Button } from 'reactstrap';

function Alert({
    open=false,
    message,
    cancelButtonColor='secondary',
    confirmButtonColor='danger',
    cancelButtonText='Cancel',
    confirmButtonText='Delete',
    onCancel,
    onConfirm=null
}) {
    const [hiddenClass, setHiddenClass] = useState('');

	const handleCancel = () => {
		setHiddenClass('hidden');
		setTimeout(() => {
            setHiddenClass('');
            onCancel();
		}, 100);
	}

    const handleConfirm = () => {
        onConfirm();
        handleCancel();
    }

    if (!open) {
        return null;
    }

	return (
		<div className={"c-alert " + hiddenClass}>
            <div className="backdrop" onClick={(e) => handleCancel(e)}></div>
            <Card>
                <div className="c-alert-header d-flex justify-content-end align-items-center">
                    <button type="button" className="close" aria-label="Close" onClick={() => handleCancel()}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div className="c-alert-body" dangerouslySetInnerHTML={{__html:message}}></div>
                
                <div className="c-alert-footer align-self-end mt-auto">
                    <Button className="mr-2" outline color={cancelButtonColor} onClick={(e) => handleCancel(e)} >{cancelButtonText}</Button>
                    {onConfirm &&
                        <Button color={confirmButtonColor} onClick={(e) => handleConfirm(e)} >{confirmButtonText}</Button>
                    }
                </div>
            </Card>
        </div>
	);
}

export default Alert;