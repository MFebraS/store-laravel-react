import React from 'react';
import { Button } from 'reactstrap';
import { ChevronLeft, ChevronRight } from 'react-feather';

function Pagination({ first, last, total, disablePrevButton, disableNextButton, onPrevClick=null, onNextClick=null }) {

	if (!first) {
		return null;
	}

	return (
        <div className="links d-flex justify-content-end align-items-center mt-3">
            <div className="mr-3">
                {first} - {last} entries of {total}
            </div>
            <Button className="mr-2" outline color="secondary" disabled={disablePrevButton}
                onClick={() => onPrevClick()}
            >
                <ChevronLeft color="#8c929d" size={16} />
            </Button>
            <Button outline color="secondary" disabled={disableNextButton}
                onClick={() => onNextClick()}
            >
                <ChevronRight color="#8c929d" size={16} />
            </Button>
        </div>
	);
}

export default Pagination;