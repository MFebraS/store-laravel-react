import React, { useEffect, useState, useRef, useMemo } from 'react';
import { Marker, Popup, useMap, useMapEvents } from 'react-leaflet';
import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: process.env.MIX_APP_URL + 'public/images/leaflet/marker-icon-2x.png',
    iconUrl: process.env.MIX_APP_URL + 'public/images/leaflet/marker-icon.png',
    shadowUrl: process.env.MIX_APP_URL + 'public/images/leaflet/marker-shadow.png',
});

function DraggableMarker({ center, onDragend=null }) {
    const [position, setPosition] = useState(center);
    const markerRef = useRef(null);
    const map = useMap();

    useEffect(() => {
        onDragend(position);
    }, [position]);

    const eventHandlers = useMemo(() => ({
        dragend() {
            const marker = markerRef.current;
            if (marker != null) {
                setPosition(marker.getLatLng());
            }
        }
    }), []);

	const mapEvents = useMapEvents({
		click: (e) => {
			setPosition(e.latlng);
		},
		moveend: (e) => {
			setPosition(map.getCenter());
		},
	})

    return (
        <Marker
            draggable={true}
            eventHandlers={eventHandlers}
            position={position}
            ref={markerRef}
        >
            <Popup minWidth={90}>
                <span>Marker</span>
            </Popup>
        </Marker>
    )
}

export default DraggableMarker;