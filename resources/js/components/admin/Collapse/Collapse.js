import React, { Fragment, useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

function Collapse({
    titleClass,
    bodyClass,
    title,
    body,
    open = false,
    renderTitle = title => null,
    renderBody = body => null
}) {
    const [isOpen, setIsOpen] = useState(open);
    const [childHeight, setChildHeight] = useState(0);
    const collapse_elm = useRef();

    useEffect(() => {
        const childHeightRaw = collapse_elm.current.clientHeight;
        setChildHeight(`${childHeightRaw / 16}rem`);
    }, [])

    const toggle = () => setIsOpen(!isOpen);

    return (
        <Fragment>
            <div className={titleClass} onClick={() => toggle()}>
                {renderTitle(title)}
            </div>
            <div className={'c-collapse ' + bodyClass} style={{ maxHeight: isOpen ? childHeight : 0 }} >
                <div ref={collapse_elm} >
                    {renderBody(body)}
                </div>
            </div>
        </Fragment>
    );
}

export default Collapse;
